from asyncio.log import logger
import os
import sys
from typing import List, Optional
from fastapi.concurrency import run_in_threadpool
from pyrate_limiter import Duration, Rate, Limiter, BucketFullException
from fastapi.responses import ORJSONResponse, Response
from fastapi.middleware.cors import CORSMiddleware
from io import BytesIO
import logging
import orjson
import hmac
from fastapi.staticfiles import StaticFiles
from sqlalchemy.future import select
from sqlalchemy import Integer, cast, create_engine, Table, MetaData, func, update
from sqlalchemy.orm import sessionmaker
import pandas as pd
import tracemalloc
from model import (LoggingModel, QueryModel)
from model.superDeliver.GeolocationModel import (StoreCoordinates)
from model.superLocator import (LocatorModel)
from controller.superStatement import (
    StatementController
)
from model.superStatement.StatementModel import (
    ClientStatement,
    ClientStatementBillCreate,
    ClientStatementBillResponse,
    EmailData,
    LoginStatementResponse,
    LoginStatementCreate
)
from model.superStatement.CustomerModel import (
    CustomerInfo, 
    CustomerInfoModel, 
    CustomerInfoResponse
)
from model.superDeliver import (
    DriverModel, 
    DriverOrderModel, 
    TentativeModel
)
from model.superStatement.TemplatesModel import (
    HTMLContent, 
    HTMLTemplate
)
from controller.superDeliver import DriverController, DriverOrderController, GeoController, ImageController
from controller.superLocator import LocatorController
from controller.superStatement import EmailController
from utility.util import (
    get_primary_db,
    get_secondary_db,
    AsyncSession,
    APP_ADMIN_KEY,
    DISPATCH_ADMIN_KEY,
    STATE_OF_DEPLOYMENT,
)
from fastapi import (
    FastAPI,
    Query,
    Request,
    Depends,
    File,
    UploadFile,
    status,
    APIRouter,
    HTTPException,
)

# Initialize the Limiter
login_limiter = Limiter(Rate(96, Duration.DAY))

# Declare the FastAPI app
app = FastAPI(
    debug=False,
    docs_url=None,
    redoc_url=None,
)

# Log the app start and print the state of deployment
logging.warning(
    f"|- ({STATE_OF_DEPLOYMENT}) ============================================== ({STATE_OF_DEPLOYMENT}) -|"
)
logging.warning(
    f"|- ({STATE_OF_DEPLOYMENT}) |-------- NOTICE: Api is now running --------| ({STATE_OF_DEPLOYMENT}) -|"
)
logging.warning(
    f"|- ({STATE_OF_DEPLOYMENT}) ============================================== ({STATE_OF_DEPLOYMENT}) -|"
)
tracemalloc.start()

# Set the event loop policy for Windows
if sys.platform == "win32":
    import asyncio
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    
# =============================== Middleware ===================================#

def authenticate(request: Request):
    if not DriverController.authenticate(
        request, request.headers.get("X-Deliver-Auth")
    ):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Non autorisé"
        )

def is_app_admin(request: Request):
    if not hmac.compare_digest(request.headers.get("X-Admin-Key"), APP_ADMIN_KEY):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Non autorisé"
        )


def is_dispatch(request: Request):
    if not hmac.compare_digest(
        request.headers.get("X-Dispatch-Key"), DISPATCH_ADMIN_KEY
    ):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Non autorisé"
        )


# Middleware to get driver_id from headers
async def get_driver_id(request: Request):
    driver_id = request.headers.get("X-Driver-Id")
    if not driver_id:
        raise HTTPException(status_code=400, detail="Driver ID not provided")
    request.state.driver_id = driver_id

# Cors Middleware
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Define the paths
BASE_PATHS_CSV = {
    "prod": "/home/images/csv/",
    "dev": "/home/images-dev/csv/",
    "local": "csv/",
}

BASE_PATHS_TEMPLATES = {
    "prod": "/home/images/public/",
    "dev": "/home/images-dev/public/",
    "local": "public/",
}

BASE_PATHS_DB = {
    "prod": "mysql+pymysql://deliver:aSNNlI5QbAELCso@127.0.0.1:3306/pasuperxyz_superdeliver",
    "dev": "mysql+pymysql://deliver-dev:2TLwXg8B75myHlI@127.0.0.1:3306/pasuperxyz_superdeliver_dev",
    "local": "mysql+pymysql://root:Pasuper7803!@127.0.0.1:3306/pasuperxyz_superdeliver",
}

BASE_PATHS_IMG = {
    "prod": "/home/images",
    "dev": "/home/images-dev",
    "local": "images",
}

app.mount(
    "/images",
    StaticFiles(directory=BASE_PATHS_IMG.get(STATE_OF_DEPLOYMENT)),
    name="images",
)

app.mount(
    "/ftp-user",
    StaticFiles(directory=BASE_PATHS_CSV.get(STATE_OF_DEPLOYMENT)),
    name="ftp-user",
)

# Declare the specific APIRouters with middleware
register_router = APIRouter(
    prefix="/register",
)
driver_router = APIRouter(
    prefix="/driver",
    dependencies=[Depends(is_dispatch)],
)
driver_order_router = APIRouter(
    prefix="/driver_order",
    dependencies=[Depends(authenticate)],
)
geo_router = APIRouter(
    prefix="/geo",
    dependencies=[Depends(authenticate)],
)
image_router = APIRouter(
    prefix="/save_images",
    dependencies=[Depends(authenticate)],
)
admin_router = APIRouter(
    prefix="/admin",
    dependencies=[Depends(authenticate), Depends(is_app_admin)],
)
dispatch_router = APIRouter(
    prefix="/misc_dispatch",
    dependencies=[Depends(is_dispatch)],
)
statement_router = APIRouter(
    prefix="/statement",
    dependencies=[Depends(is_dispatch)],
)

# ======================== Store Coords ======================================#
# Defining the coordinates as a dictionary
store_coordinates = {
    1: '45.48750046461106,-73.38457638559589',
    2: '45.33034882948999,-73.29479794494063',
    3: '45.35040656602404,-73.68937198884842',
}

@app.get("/store_coordinates", response_model=StoreCoordinates)
async def get_store_coordinates():
    return {"coordinates": store_coordinates}

# =========================SUPER DELIVER =====================================#

@register_router.post("/login")
async def login(
    data: DriverModel.DriverLogin,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await DriverController.login(data, db)

@driver_router.post("/get_drivers")
async def get_drivers(
    data: DriverModel.StoreId,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await DriverController.get_all_drivers(data, db)

@driver_router.post("/get_driver")
async def get_driver(
    data: DriverModel.DriverId,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await DriverController.get_driver(data, db)


@driver_router.post("/create_driver")
async def create_driver(
    data: DriverModel.DriverCreateOrUpdate,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await DriverController.create_driver(data, db)


@driver_router.post("/update_driver")
async def update_driver(
    data: DriverModel.DriverCreateOrUpdate,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await DriverController.update_driver(data, db)


@driver_router.post("/activate")
async def activate_driver(
    data: DriverModel.DriverId,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await DriverController.driver_status(data, db, activate=True)


@driver_router.post("/deactivate")
async def deactivate_driver(
    data: DriverModel.DriverId,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await DriverController.driver_status(data, db, activate=False)

@driver_order_router.get("/get_routes_order")
async def get_routes_order(
    request: Request,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await DriverOrderController.get_routes_order(
        request.state.driver_id, db
    )

@driver_order_router.get("/get_driver_orders")
async def get_driver_orders(
    request: Request,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await DriverOrderController.get_driver_orders(request.state.driver_id, db)

@driver_order_router.post("/get_latest_route", status_code=status.HTTP_200_OK)
async def get_latest_route(
    data: DriverModel.DriverId,
    session: AsyncSession = Depends(get_primary_db)
) -> ORJSONResponse:
    try:
        # Query to get the latest route for the given driver_id
        query = (
            select(DriverOrderModel.DriverOrder.route)
            .where(DriverOrderModel.DriverOrder.driver_id == data.driver_id)
            .order_by(
                cast(
                    func.substring_index(DriverOrderModel.DriverOrder.route, '-', -1), Integer
                ).desc()
            )
            .limit(1)
        )
        
        result = await session.execute(query)
        latest_route = result.scalar()

        if latest_route is None:
            latest_route_number = 0
        else:
            # Extract the route number part from the format "driver_id-<route_number>"
            try:
                latest_route_number = int(latest_route.split("-")[1])
            except (ValueError, IndexError):
                latest_route_number = 0  # Fallback if parsing fails

        return ORJSONResponse({"latest_route": latest_route_number})
    
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(e))

@driver_order_router.post("/reorder_driver_orders")
async def reorder_driver_orders(
    request: Request,
    data: List[DriverOrderModel.OrderReorderRequest],
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    driver_id = request.state.driver_id
    return await DriverOrderController.reorder_driver_orders(driver_id, data, db)

@driver_order_router.post("/set_driver_order")
async def set_driver_order(
    request: Request,
    data: DriverOrderModel.InvoiceCode,
    db: AsyncSession = Depends(get_primary_db),
    db_sec: AsyncSession = Depends(get_secondary_db),
) -> ORJSONResponse:
    # Extract the route from the request body
    body = await request.json()
    route = body.get('route')
    received_by = body.get('received_by') 

    return await DriverOrderController.set_driver_order(
        request.state.driver_id, data, db, db_sec, route, received_by,
    )

@driver_order_router.post("/received_by")
async def received_by(
    data: DriverOrderModel.ReceivedBy,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    response = await DriverOrderController.received_by(data, db)
    return ORJSONResponse(content=response)

@driver_order_router.post("/set_to_arrived")
async def set_to_arrived(
    data: DriverOrderModel.InvoiceInfo,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await DriverOrderController.set_to_arrived(data, db)

@driver_order_router.post("/set_to_delivered")
async def set_to_delivered(
    data: DriverOrderModel.InvoiceInfo,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await DriverOrderController.set_to_delivered(data, db)

@driver_order_router.post("/scan_part")
async def scan_part(
    data: DriverOrderModel.ScannedPart,
    db: AsyncSession = Depends(get_primary_db),
    db_sec: AsyncSession = Depends(get_secondary_db),
) -> ORJSONResponse:
    return await DriverOrderController.scan_part(
        data, db, db_sec, scan_type="num_scanned"
    )

@driver_order_router.post("/scan_part_confirmed")
async def scan_part_confirmed(
    data: DriverOrderModel.ScannedPart,
    db: AsyncSession = Depends(get_primary_db),
    db_sec: AsyncSession = Depends(get_secondary_db),
) -> ORJSONResponse:
    return await DriverOrderController.scan_part(
        data, db, db_sec, scan_type="confirmed_scanned"
    )

@driver_order_router.post("/batch_scan")
async def batch_scan(
    request: Request,
    data: DriverOrderModel.ScannedBatch,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await DriverOrderController.batch_scan(request.state.driver_id, data, db)

@driver_order_router.post("/skip_part_at_delivery")
async def skip_part_at_delivery(
    request: Request,
    data: DriverOrderModel.ScannedBatch,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await DriverOrderController.skip_part_at_delivery(
        request.state.driver_id, data, db
    )

@driver_order_router.post("/remove_active_driver_order")
async def remove_active_driver_order(
    data: DriverOrderModel.InvoiceInfo,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await DriverOrderController.remove_driver_order(data, db, is_dispatch=False)

@driver_order_router.post("/retour_driver_orders")
async def retour_driver_orders(
    data: TentativeModel.TentativeOrder,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await DriverOrderController.retour_driver_orders(data, db, is_dispatch=False)

@driver_order_router.post("/cancel_driver_orders")
async def cancel_driver_orders(
    data: TentativeModel.TentativeOrder,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await DriverOrderController.cancel_driver_orders(data, db, is_dispatch=False)

@driver_order_router.post("/start_route", status_code=status.HTTP_200_OK)
async def start_route(request: DriverOrderModel.StartRouteRequest, session: AsyncSession = Depends(get_primary_db)):
    try:
        # Update all orders in the route to set route_started to True
        stmt = update(DriverOrderModel.DriverOrder).where(DriverOrderModel.DriverOrder.route == request.route).values(route_started=True)
        await session.execute(stmt)
        await session.commit()

        return {"detail": "Route started successfully"}
    except Exception as e:
        await session.rollback()
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(e))

@geo_router.post("/set_driver_telemetry")
async def set_driver_telemetry(
    request: Request,
    data: LoggingModel.TelemetryRequest,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await GeoController.set_driver_telemetry(request.state.driver_id, data, db)


@geo_router.post("/set_route_deviation")
async def set_route_deviation(
    request: Request,
    data: LoggingModel.RouteDeviationRequest,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await GeoController.set_route_deviation(request.state.driver_id, data, db)

@image_router.post("/signature")
async def signatures(
    file: UploadFile = File(...),
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await ImageController.store_image(file, db, type="signatures")


@image_router.post("/photo")
async def photos(
    file: UploadFile = File(...),
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await ImageController.store_image(file, db, type="photos")

@dispatch_router.post("/get_all_deliveries_time")
async def get_deliveries_time(
    data: DriverModel.StoreId,
    db: AsyncSession = Depends(get_primary_db),
) -> Response:
    return await DriverOrderController.get_all_deliveries_time(
        data.store, db
    )

@dispatch_router.post("/get_deliveries_time")
async def get_deliveries_time(
    data: DriverModel.DeliveryTimeRequest,
    db: AsyncSession = Depends(get_primary_db),
) -> Response:
    return await DriverOrderController.get_deliveries_time(
        data.store_id, data.driver_id, db
    )

@dispatch_router.post("/get_quantity_orders")
async def get_quantity_orders(
    data: DriverModel.StoreId,
    params: Optional[QueryModel.QueryParams] = None,
    db_primary: AsyncSession = Depends(get_primary_db),
    db_secondary: AsyncSession = Depends(get_secondary_db),
) -> Response:
    return await DriverOrderController.get_quantity_orders(data, params, db_primary, db_secondary)

@dispatch_router.post("/get_all_orders")
async def get_all_orders(
    data: DriverModel.StoreId,
    db_primary: AsyncSession = Depends(get_primary_db),
    db_secondary: AsyncSession = Depends(get_secondary_db),
) -> Response:
    return await DriverOrderController.get_all_orders(data, db_primary, db_secondary)

@dispatch_router.post("/get_order_by_number")
async def get_order_by_number(
    data: DriverOrderModel.DriverOrderNumber,
    db_primary: AsyncSession = Depends(get_primary_db),
    db_secondary: AsyncSession = Depends(get_secondary_db),
) -> Response:
    return await DriverOrderController.get_order_by_number(data, db_primary, db_secondary)

@dispatch_router.post("/get_all_orders_by_id")
async def get_all_orders_by_id(
    data: DriverModel.DriverId,
    db: AsyncSession = Depends(get_primary_db),
    db_secondary: AsyncSession = Depends(get_secondary_db),
) -> Response:
    return await DriverOrderController.get_all_orders_by_id(data, db,db_secondary)

@dispatch_router.post("/get_all_routes_by_driverId")
async def get_all_routes_by_driverId(
    data: DriverModel.DriverIdRoute,
    db: AsyncSession = Depends(get_primary_db),
) -> Response:
    return await DriverOrderController.get_all_routes_by_driverId(data, db)

@dispatch_router.post("/remove_any_driver_order")
async def remove_any_driver_order(
    data: DriverOrderModel.InvoiceInfo,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await DriverOrderController.remove_driver_order(data, db, is_dispatch=True)

@dispatch_router.post("/remove_cancel_order")
async def remove_cancel_order(
    data: DriverOrderModel.InvoiceInfo,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await DriverOrderController.remove_cancel_order(data, db)

@dispatch_router.get("/get_route")
async def get_route_endpoint(
    origin: str = Query(..., description="Origin coordinates (latitude,longitude)"),
    destination: str = Query(..., description="Destination coordinates (latitude,longitude)"),
    departureTime: str = Query(None, description="Optional departure time"),
) -> ORJSONResponse:
    response = await GeoController.get_route(origin, destination, departureTime)
    return ORJSONResponse(content=response)

@dispatch_router.post("/get_oauth_token")
async def get_oauth_token_endpoint():
    token_data = await run_in_threadpool(GeoController.get_oauth_token)  # Run the synchronous function in a thread pool
    return ORJSONResponse(content=token_data)

# ============================END SUPER DELIVER =====================================================#

# ============================SUPER LOCATOR =====================================================#
@dispatch_router.post("/create_locator")
async def create_locator(
    data: LocatorModel.LocatorCreate,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await LocatorController.create_locator(data, db)

@register_router.post("/login_locator")
async def login_locator(
    data: LocatorModel.LocatorLogin,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await LocatorController.login_locator(data, db)





# ============================END SUPER LOCATOR =====================================================#

# ============================SUPER STATEMENT =====================================================#

@register_router.post("/loginStatement", response_model=LoginStatementResponse)
async def loginStatement(
    data: LoginStatementCreate,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:

   return await StatementController.loginStatement(data, db)
@statement_router.post("/create_user", response_model=LoginStatementResponse)
async def create_user(
    data: LoginStatementCreate,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await StatementController.create_user(data, db)

@statement_router.get("/list_statements", response_model=List[ClientStatementBillResponse])
async def list_statements(
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
  
    try:
        result = await db.execute(select(ClientStatement))
        statements = result.scalars().all()
        statements_response = [ClientStatementBillResponse.from_orm(statement).dict() for statement in statements]
        return ORJSONResponse(
            status_code=status.HTTP_200_OK,
            content={"statements": statements_response}
        )
    except Exception as e:
        logging.warning(f"Database error: {e}")
        return ORJSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"detail": "Database error."}
        )

@statement_router.post("/create_client", response_model=CustomerInfoResponse)
async def create_client(
    data: CustomerInfoModel,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    return await StatementController.create_client(data, db)

@statement_router.put("/update_client/{customer_number}", response_model=CustomerInfoResponse)
async def update_client(
    customer_number: str,
    data: CustomerInfoModel,
    db: AsyncSession = Depends(get_primary_db),
) -> CustomerInfoResponse:
    return await StatementController.update_client(customer_number, data, db)

@statement_router.get("/get_client/{customer_number}", response_model=CustomerInfoResponse)
async def get_client(customer_number: str, db: AsyncSession = Depends(get_primary_db)):
    async with db.begin():
        result = await db.execute(
            select(CustomerInfo).filter(CustomerInfo.customer_number == customer_number)
        )
        client = result.scalars().first()
        if not client:
            raise HTTPException(status_code=404, detail="Client not found")
        return client
    
@app.post("/get_email", response_model=List[EmailData])
async def get_email_endpoint(
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    emails = await EmailController.check_email(db)
    return ORJSONResponse({"emails": emails})  
    
@app.post("/send_email", response_model=ClientStatementBillResponse)
async def send_email_endpoint(
    data: ClientStatementBillCreate,
    db: AsyncSession = Depends(get_primary_db),
) -> ORJSONResponse:
    logger.info(f"Received request data: {data}")
    await StatementController.process_send_email(data, db)
    return ORJSONResponse({"message": "Emails sent successfully"})

@app.get("/fetch-html")
async def fetch_html(fileName: str, db: AsyncSession = Depends(get_primary_db)):
    async with db as session:
        result = await session.execute(select(HTMLTemplate).filter(HTMLTemplate.name == fileName))
        html_template = result.scalars().first()
    if html_template is None:
        raise HTTPException(status_code=404, detail="File not found")
    return {"html": html_template.content}

@app.post("/save-html")
async def save_html(content: HTMLContent, db: AsyncSession = Depends(get_primary_db)):
    async with db as session:
        result = await session.execute(select(HTMLTemplate).filter(HTMLTemplate.name == content.fileName))
        html_template = result.scalars().first()
        if html_template:
            html_template.content = content.html
        else:
            html_template = HTMLTemplate(name=content.fileName, content=content.html)
            session.add(html_template)
        await session.commit()
        await session.refresh(html_template)
    return {"message": "File saved successfully"}

@app.post("/run-script")
def run_script():
    try:
        csv_path = BASE_PATHS_CSV.get(STATE_OF_DEPLOYMENT)
        if not csv_path:
            raise ValueError("CSV path not found for the current deployment state")
        csv_file = os.path.join(csv_path, 'customers.csv')  # Adjust the file name as necessary
        csv_db_path = BASE_PATHS_DB.get(STATE_OF_DEPLOYMENT)
        if not csv_db_path:
            raise ValueError("Database path not found for the current deployment state")

        if not os.path.exists(csv_file):
            raise FileNotFoundError(f"CSV file not found: {csv_file}")

        df = pd.read_csv(csv_file)

        DATABASE_URL = csv_db_path
        engine = create_engine(DATABASE_URL)
        Session = sessionmaker(bind=engine)
        session = Session()

        metadata = MetaData()
        customer_info = Table('customer_info', metadata, autoload_with=engine)

        with engine.begin() as connection:
            for index, row in df.iterrows():
                select_stmt = select(customer_info.c.customer_number).where(customer_info.c.customer_number == row['Customer Number'])
                result = connection.execute(select_stmt).fetchone()

                if result:
                    update_stmt = customer_info.update().where(customer_info.c.customer_number == row['Customer Number']).values(
                        customer_name=row['Customer Name'],
                        customer_number=row['Customer Number'],
                        running_balance=row['Running Balance'],
                        sequence=row['Sequence'],
                        email_address=row['Email Address'],
                    )
                    connection.execute(update_stmt)
                else:
                    insert_stmt = customer_info.insert().values(
                        customer_name=row['Customer Name'],
                        customer_number=row['Customer Number'],
                        running_balance=row['Running Balance'],
                        sequence=row['Sequence'],
                        email_address=row['Email Address'],
                    )
                    connection.execute(insert_stmt)

        session.commit()
        session.close()

        return {"message": "Script executed successfully"}
    except FileNotFoundError as e:
        print(f"FileNotFoundError: {str(e)}")
        raise HTTPException(status_code=404, detail=str(e))
    except ValueError as e:
        print(f"ValueError: {str(e)}")
        raise HTTPException(status_code=400, detail=str(e))
    except Exception as e:
        print(f"Exception: {str(e)}")
        raise HTTPException(status_code=500, detail=str(e))
    
@app.get("/fetch-customer-data")
def fetch_customer_data():
    try:
        DATABASE_URL = BASE_PATHS_DB.get(STATE_OF_DEPLOYMENT)
        if not DATABASE_URL:
            raise ValueError("Database path not found for the current deployment state")

        engine = create_engine(DATABASE_URL)
        metadata = MetaData()
        customer_info = Table('customer_info', metadata, autoload_with=engine)

        with engine.connect() as connection:
            select_stmt = select(
                customer_info.c.customer_number,
                customer_info.c.customer_name,
                customer_info.c.running_balance,
                customer_info.c.email_address,
                customer_info.c.ready,
                customer_info.c.sent,
                customer_info.c.follow_up
            ).where(customer_info.c.running_balance > 0)
            result = connection.execute(select_stmt)
            customers = [dict(row._mapping) for row in result]

        return customers
    except Exception as e:
        print(f"Error: {str(e)}")
        raise HTTPException(status_code=500, detail=str(e))
    
# ============================ END SUPER STATEMENT =====================================================#
    
# ============================ INCLUDE ROUTERS =====================================================#

app.include_router(register_router)
app.include_router(driver_router)
app.include_router(driver_order_router)
app.include_router(geo_router)
app.include_router(image_router)
app.include_router(admin_router)
app.include_router(dispatch_router)
app.include_router(statement_router)