from asyncio import exceptions
from cryptography.hazmat.primitives.ciphers.aead import AESGCM
from fastapi.responses import ORJSONResponse
from datetime import timedelta, datetime
from argon2 import PasswordHasher, Type
from sqlalchemy.future import select
from fastapi import status, Request, Depends
from sqlalchemy import and_
import logging
import base64
import orjson
import hmac
import os
from model.superDeliver.DriverModel import (
    Driver, 
    DriverCreateOrUpdate, 
    DriverLogin, 
    DriverId, 
    StoreId
)
from utility.util import (
    AsyncSession,
    PRIVATE_KEY,
    JWE_SECRET_KEY,
    PUBLIC_KEY,
    ARGON2_SECRET_KEY,
    DEFAULT_VEHICULE,
    HERE_SDK_KEY_ID,
    HERE_SDK_KEY,
    get_primary_db,
)


# Authentication / Security Utilities
def argon2_strong_hash(password: str) -> str:
    # Create the peppered password
    peppered_password = f"{password}{ARGON2_SECRET_KEY}"

    # Create a PasswordHasher with custom parameters
    hasher = PasswordHasher(
        time_cost=1,
        memory_cost=65536,
        parallelism=1,
        hash_len=32,
        salt_len=16,
        type=Type.ID,
    )

    # Hash the password
    hash = hasher.hash(peppered_password)
    return hash


def create_jwe(driver_id: int) -> str:
    # Calculate the date one day from now
    expiration_date = datetime.utcnow() + timedelta(days=1)
    expiration_date = expiration_date.strftime("%Y%m%d")

    # Create the claims
    claims = {"sub": driver_id, "exp": expiration_date}

    # Create a JWT
    payload_b85 = base64.b85encode(orjson.dumps(claims)).decode()
    jwt = f"{payload_b85}"

    # Sign the JWT with Ed25519
    signature = PRIVATE_KEY.sign(jwt.encode())
    signature_b85 = base64.b85encode(signature).decode()
    signed_jwt = f"{jwt}.{signature_b85}"

    # Generate a random nonce
    nonce = os.urandom(12)

    # Encrypt the signed JWT
    aesgcm = AESGCM(JWE_SECRET_KEY)
    ciphertext = aesgcm.encrypt(nonce, signed_jwt.encode(), None)

    # Create the JWE object
    iv_b85 = base64.b85encode(nonce).decode()
    ciphertext_b85 = base64.b85encode(ciphertext).decode()
    jwe = f"{iv_b85}.{ciphertext_b85}"
    return jwe


def authenticate(request: Request, jwe: str) -> bool:
    try:
        # Split the JWE into IV and ciphertext
        iv_b85, ciphertext_b85 = jwe.split(".")
        iv = base64.b85decode(iv_b85)
        ciphertext = base64.b85decode(ciphertext_b85)

        # Decrypt the JWE
        aesgcm = AESGCM(JWE_SECRET_KEY)
        signed_jwt = aesgcm.decrypt(iv, ciphertext, None).decode()

        # Split the signed JWT into payload and signature
        payload_b85, signature_b85 = signed_jwt.rsplit(".", 1)
        signature = base64.b85decode(signature_b85)

        # Verify the signature using Ed25519
        PUBLIC_KEY.verify(signature, payload_b85.encode())

        # Decode the payload
        payload = orjson.loads(base64.b85decode(payload_b85))

        # Extract the expiration date from the payload and convert it from 'YYYYMMDD' format string to a datetime object
        expiration_date = datetime.strptime(payload["exp"], "%Y%m%d")

        # Compare the current UTC datetime with the expiration datetime
        if datetime.utcnow().date() >= expiration_date.date():
            return False

        # Set the driver_id in the request state
        request.state.driver_id = int(payload["sub"])
        return True
    except Exception as e:
        print(f"Authentication failed: {e}")
        return False

def verify_password(stored_password: str, provided_password: str) -> bool:
    peppered_password = f"{provided_password}{ARGON2_SECRET_KEY}"
    try:
        hasher = PasswordHasher()
        hasher.verify(stored_password, peppered_password)
        return True
    except exceptions.VerifyMismatchError:
        return False

async def login(
    validated_data: DriverLogin,
    db: AsyncSession,
) -> ORJSONResponse:
    try:
        # Asynchronously query the database
        result = await db.execute(
            select(Driver).where(
                and_(
                    Driver.username == validated_data.username,
                    Driver.is_active == 1,
                )
            ).limit(1)
        )
        user = result.scalars().first()

        if not user:
            logging.warning(f"No user found: {validated_data.username}")
            return ORJSONResponse(
                status_code=status.HTTP_401_UNAUTHORIZED,
                content={"detail": "Nom d'utilisateur ou mot de passe incorrect."},
            )

        # Verify the password
        if not verify_password(user.password, validated_data.password):
            logging.warning(f"Invalid password for: {validated_data.username}")
            return ORJSONResponse(
                status_code=status.HTTP_401_UNAUTHORIZED,
                content={"detail": "Nom d'utilisateur ou mot de passe incorrect."},
            )

        await db.commit()
        await db.refresh(user)

        token = create_jwe(user.id)

        return ORJSONResponse(
            status_code=status.HTTP_200_OK,
            content={
                "token": token,
                "sdk_key_id": HERE_SDK_KEY_ID,
                "sdk_key": HERE_SDK_KEY,
                "storeId": user.store, 
                "driverId": user.id, 
            },
        )
    except Exception as e:
        await db.rollback()
        logging.warning(f"Database error for: {validated_data.username} - {e}")
        return ORJSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"detail": "Nom d'utilisateur ou mot de passe incorrect."},
        )
    
async def get_driver(driver_id: int, db: AsyncSession = Depends(get_primary_db)):
    try:
        result = await db.execute(select(Driver).filter(Driver.id == driver_id))
        driver = result.scalars().first()

        if not driver:
            return ORJSONResponse(
                status_code=status.HTTP_404_NOT_FOUND,
                content={"detail": "No drivers found."},
            )

        return ORJSONResponse(
                content={"drivers": driver},
                status_code=status.HTTP_200_OK,
            )
    except Exception as e:
        await db.rollback()
        logging.error(f"Database error in retrieving drivers: {e}")
        return ORJSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"detail": "Unknown database error."},
        )
    
async def get_all_drivers(
        validated_data: StoreId,
        db: AsyncSession) -> ORJSONResponse:
    try:
       
        result = await db.execute(select(Driver)
                    .where(
                        Driver.store == validated_data.store,
                    )
                )
        drivers = result.scalars().all()

        if not drivers:
            return ORJSONResponse(
                status_code=status.HTTP_404_NOT_FOUND,
                content={"detail": "No drivers found."},
            )

        drivers_data = [
                {
                    "id": driver.id,
                    "is_active": driver.is_active,
                    "username": driver.username,
                    "store": driver.store,
                    "created_at": driver.created_at,
                }
                for driver in drivers
            ] 

        return ORJSONResponse(
            content={"drivers": drivers_data},
            status_code=status.HTTP_200_OK,
        )
    except Exception as e:
        await db.rollback()
        logging.error(f"Database error in retrieving drivers: {e}")
        return ORJSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"detail": "Unknown database error."},
        )

async def get_driver(
       validated_data: DriverId,
        db: AsyncSession) -> ORJSONResponse: 
    try:
       
        result = await db.execute(select(Driver)
                    .where(
                        Driver.id == validated_data.driver_id,
                    ).limit(1)
                )
        driver = result.scalars().first()

        if not driver:
            return ORJSONResponse(
                status_code=status.HTTP_404_NOT_FOUND,
                content={"detail": "No drivers found."},
            )

        driver_data = {
                    "id": driver.id,
                    "is_active": driver.is_active,
                    "username": driver.username,
                    "store": driver.store,
                    "created_at": driver.created_at,
                }

        return ORJSONResponse(
            content={"drivers": driver_data},
            status_code=status.HTTP_200_OK,
        )
    except Exception as e:
        await db.rollback()
        logging.error(f"Database error in retrieving drivers: {e}")
        return ORJSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"detail": "Unknown database error."},
        )

async def create_driver(
    validated_data: DriverCreateOrUpdate,
    db: AsyncSession,
) -> ORJSONResponse:
    try:
        # Asynchronously check if the username already exists
        result = await db.execute(
            select(Driver).where(Driver.username == validated_data.username).limit(1)
        )
        if result.scalars().first():
            return ORJSONResponse(
                status_code=status.HTTP_400_BAD_REQUEST,
                content={"detail": "Nom d'utilisateur déjà utilisé."},
            )

        hashed_password = argon2_strong_hash(validated_data.password)

        new_driver = Driver(
            username=validated_data.username,
            password=hashed_password,
            store=validated_data.store,
            is_active=True,
        )

        db.add(new_driver)
        await db.commit()

        return ORJSONResponse(
            content={"detail": "Livreurs créé!"},
            status_code=status.HTTP_201_CREATED,
        )
    except Exception as e:
        await db.rollback()
        logging.warning(f"Database error in create driver: {e}")
        return ORJSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"detail": "Erreur inconnue dans la base de données."},
        )


async def update_driver(
    validated_data: DriverCreateOrUpdate,
    db: AsyncSession,
) -> ORJSONResponse:
    try:
        result = await db.execute(
            select(Driver).where(Driver.id == validated_data.driver_id).limit(1),
        )
        driver = result.scalars().first()

        if not driver:
            return ORJSONResponse(
                status_code=status.HTTP_404_NOT_FOUND,
                content={"detail": "Livreurs non trouvé."},
            )

        driver.password = argon2_strong_hash(validated_data.password)
        driver.store = validated_data.store

        await db.commit()
        await db.refresh(driver)

        return ORJSONResponse(
            content={"detail": "Livreurs mis à jour!"},
            status_code=status.HTTP_200_OK,
        )
    except Exception as e:
        logging.warning(f"Error in update_driver: {e}")
        return ORJSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"detail": "Une erreur inconnue s'est produite."},
        )


async def driver_status(
    validated_data: DriverId,
    db: AsyncSession,
    activate: bool = True,
) -> ORJSONResponse:
    try:
        result = await db.execute(
            select(Driver).where(Driver.id == validated_data.driver_id).limit(1),
        )
        driver = result.scalars().first()

        # Check if the driver exists
        if not driver:
            return ORJSONResponse(
                status_code=status.HTTP_404_NOT_FOUND,
                content={"detail": "Livreur non trouvé."},
            )

        # Activate or deactivate the driver
        message_ok = "Activation" if activate else "Déactivation"
        message_not_ok = "l'activation" if activate else "la déactivation"
        driver.is_active = True if activate else False

        await db.commit()
        await db.refresh(driver)

        return ORJSONResponse(
            content={"detail": f"{message_ok} avec succès de {driver.username}."},
            status_code=status.HTTP_200_OK,
        )
    except Exception as e:
        logging.warning(f"Error in driver_status: {e}")
        return ORJSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"detail": f"Échec lors de {message_not_ok} de {driver.username}."},
        )
