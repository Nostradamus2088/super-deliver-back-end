from fastapi.responses import ORJSONResponse
import httpx
from utility.util import HERE_API_KEY, DISPATCH_ADMIN_KEY, HERE_SDK_KEY, HERE_SDK_KEY_ID, AsyncSession
from utility.util import AsyncSession
from utility.util import HERE_API_KEY
from datetime import datetime
from fastapi import HTTPException, status
import logging
from model.LoggingModel import (
    RouteDeviation,
    RouteDeviationRequest,
    TelemetryRequest,
    Telemetry,
)

async def set_driver_telemetry(
    driver_id: int,
    validated_data: TelemetryRequest,
    db: AsyncSession,
) -> ORJSONResponse:
    try:
        # Fetch the clean address from the HERE API
        telemetry_data = Telemetry(
            driver_id=driver_id,
            order_number=validated_data.order_number,
            store=validated_data.store,
            speed=validated_data.speed,
            latitude=validated_data.latitude,
            longitude=validated_data.longitude,
            created_at=int(datetime.utcnow().timestamp()),
        )

        # Add the telemetry data to the database
        db.add(telemetry_data)
        await db.commit()

        # Return a success response
        return ORJSONResponse(
            content={"detail": "Ok"},
            status_code=status.HTTP_200_OK,
        )
    except Exception as e:
        await db.rollback()
        logging.warning(f"Database error in save telemetry: {e}")
        return ORJSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"detail": "Erreur de la base de données."},
        )


async def set_route_deviation(
    driver_id: int,
    validated_data: RouteDeviationRequest,
    db: AsyncSession,
) -> ORJSONResponse:
    try:
        # Fetch the clean address from the HERE API
        route_deviation_data = RouteDeviation(
            driver_id=driver_id,
            order_number=validated_data.order_number,
            store=validated_data.store,
            latitude=validated_data.latitude,
            longitude=validated_data.longitude,
            created_at=int(datetime.utcnow().timestamp()),
        )

        # Add the telemetry data to the database
        db.add(route_deviation_data)
        await db.commit()

        # Return a success response
        return ORJSONResponse(
            content={"detail": "Ok"},
            status_code=status.HTTP_200_OK,
        )
    except Exception as e:
        await db.rollback()
        logging.warning(f"Database error in save route deviation: {e}")
        return ORJSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"detail": "Erreur de la base de données."},
        )
    
async def extract_specific_values(json_data):
    """
    Extract values for 'duration', 'length', 'arrival', and 'departure' from JSON data.
    Args:
    json_data (dict): The JSON data as a dictionary.
    Returns:
    dict: A dictionary with the extracted values.
    """
    routes = json_data.get("routes", [])
    extracted_values = {
        "duration": None,
        "length": None,
        "arrival": None,
        "departure": None
    }

    if routes:
        for section in routes[0].get("sections", []):
            summary = section.get("summary", {})
            extracted_values = {
                "duration": summary.get("duration"),
                "length": summary.get("length"),
                "arrival": section.get("arrival", {}).get("time"),
                "departure": section.get("departure", {}).get("time"),
            }
            break  # Since we only need one section, break after the first one

    return extracted_values


async def get_route(
    origin: str,
    destination: str,
    departureTime: str = None,
) -> dict:
    """
    Fetches route information from Here Maps API and extracts specific values.
    """
    url = "https://router.hereapi.com/v8/routes"
    params = {
        "transportMode": "car",
        "destination": destination,
        "return": "summary",
        "apikey": HERE_API_KEY,
        "origin": origin,
    }
    if departureTime and departureTime != 'undefined':
        params["departureTime"] = departureTime
    else:
        print('WARNING: no departure time for origin '+origin)

    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(url, params=params)
            response.raise_for_status()  # Raise exception for non-200 status codes
            json_response = response.json()  # Get JSON response
            extracted_values = await extract_specific_values(json_response)
            return extracted_values
    except httpx.RequestError as error:
        raise HTTPException(status_code=500, detail=f"An error occurred: {error}")