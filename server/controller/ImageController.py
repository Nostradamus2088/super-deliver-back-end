from utility.util import AsyncSession, STATE_OF_DEPLOYMENT
from fastapi.responses import ORJSONResponse
from PIL import Image, ImageDraw, ImageFont
from sqlalchemy.future import select
from fastapi import UploadFile
from datetime import datetime
from fastapi import status
import logging
import os
import io
from model.superDeliver.DriverOrderModel import (DriverOrder)

async def store_image(
    file: UploadFile,
    db: AsyncSession,
    type: str = "unknown",
):
    try:
        if type not in ["signatures", "photos"]:
            return ORJSONResponse(
                status_code=status.HTTP_400_BAD_REQUEST,
                content={"detail": "Mauvais type d'image."},
            )

        parts = file.filename.split("_")
        if len(parts) != 4:
            return ORJSONResponse(
                status_code=status.HTTP_400_BAD_REQUEST,
                content={"detail": "Le nom de fichier est incorrect."},
            )

        store, _, orderNumber, _ = parts

        result = await db.execute(
            select(DriverOrder)
            .where(DriverOrder.store == store, DriverOrder.order_number == orderNumber)
            .limit(1)
        )
        driver = result.scalars().first()

        if not driver:
            return ORJSONResponse(
                status_code=status.HTTP_404_NOT_FOUND,
                content={"message": "Livreur non trouvé."},
            )

        BASE_PATHS = {
            "prod": "/home/images",
            "dev": "/home/images-dev",
            "local": "images",
        }
        path_base = BASE_PATHS.get(STATE_OF_DEPLOYMENT)
        
        if path_base is None:
            raise ValueError("Invalid STATE_OF_DEPLOYMENT")

        now = datetime.now()
        directory = f"{path_base}/{type}/{now.strftime('%Y')}/{now.strftime('%m')}/{now.strftime('%d')}"
        os.makedirs(directory, exist_ok=True)

        file_path = os.path.join(directory, file.filename)
        image_data = await file.read()

        with Image.open(io.BytesIO(image_data)) as img:
            draw = ImageDraw.Draw(img)
            timestamp = now.strftime("%Y-%m-%d %H:%M:%S")
            width, height = img.size
            font = ImageFont.load_default()

            WIDTH_OFFSET = 0.02
            HEIGHT_OFFSET = 0.95 if type == "signatures" else 0.98
            FONT_SIZE = 8 if type == "signatures" else 16

            text_position = (
                width * WIDTH_OFFSET,
                height * HEIGHT_OFFSET,
            )

            bbox = draw.textbbox(
                text_position, timestamp, font=font, font_size=FONT_SIZE
            )
            draw.rectangle(bbox, fill="black")
            draw.text(text_position, timestamp, font=font, fill="white")

            if type == "signatures":
                if (
                    os.path.exists(driver.signature_filename)
                    and STATE_OF_DEPLOYMENT == "prod"
                ):
                    os.remove(driver.signature_filename)
                driver.signature_filename = file_path
            else:
                if (
                    os.path.exists(driver.photo_filename)
                    and STATE_OF_DEPLOYMENT == "prod"
                ):
                    os.remove(driver.photo_filename)
                driver.photo_filename = file_path

            img.save(file_path)
            await db.commit()

        return ORJSONResponse(
            status_code=status.HTTP_200_OK,
            content={"message": "Image enregistrée correctement."},
        )

    except Exception as e:
        logging.warning(f"Failed to save the image with timestamp: {e}")
        return ORJSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"message": "Échec de l'enregistrement de l'image."},
        )
