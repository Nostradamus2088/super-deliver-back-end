from fastapi.responses import ORJSONResponse
from controller.superDeliver.DriverController import argon2_strong_hash, create_jwe
from sqlalchemy.future import select
from sqlalchemy.ext.asyncio import AsyncSession
from fastapi import status
from sqlalchemy import and_
import logging
import hmac
from model.superLocator.LocatorModel import (
    LocatorCreate, 
    LocatorLogin, 
    UsersLocator
)

async def create_locator(
    validated_data: LocatorCreate,
    db: AsyncSession,
) -> ORJSONResponse:
    try:
        # Asynchronously check if the username already exists
        result = await db.execute(
            select(UsersLocator).where(UsersLocator.username == validated_data.username).limit(1)
        )
        if result.scalars().first():
            return ORJSONResponse(
                status_code=status.HTTP_400_BAD_REQUEST,
                content={"detail": "Nom d'utilisateur déjà utilisé."},
            )

        hashed_password = argon2_strong_hash(validated_data.password)

        new_locator = UsersLocator(
            username=validated_data.username,
            password=hashed_password,
            store=validated_data.store,
        )

        db.add(new_locator)
        await db.commit()

        return ORJSONResponse(
            content={"detail": "Localisateur créé!"},
            status_code=status.HTTP_201_CREATED,
        )
    except Exception as e:
        await db.rollback()
        logging.warning(f"Database error in create Localisateur: {e}")
        return ORJSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"detail": "Erreur inconnue dans la base de données."},
        )

async def login_locator(
    validated_data: LocatorLogin,
    db: AsyncSession,
) -> ORJSONResponse:
    try:
        # Asynchronously query the database
        result = await db.execute(
            (
                select(UsersLocator)
                .where(
                    and_(
                        UsersLocator.username == validated_data.username
                    )
                )
                .limit(1)
            )
        )
        user = result.scalars().first()

        if not user:
            logging.warning(f"No user found: {validated_data.username}")
            return ORJSONResponse(
                status_code=status.HTTP_401_UNAUTHORIZED,
                content={"detail": "Nom d'utilisateur ou mot de passe incorrect."},
            )

        # Verify the password
        hashed_password = argon2_strong_hash(validated_data.password)

        # Compare the hashed password with the stored password
        if hmac.compare_digest(hashed_password, user.password):
            logging.warning(f"Invalid password for: {validated_data.username}")
            return ORJSONResponse(
                status_code=status.HTTP_401_UNAUTHORIZED,
                content={"detail": "Nom d'utilisateur ou mot de passe incorrect."},
            )

        await db.commit()
        await db.refresh(user)

        token = create_jwe(user.id)

        return ORJSONResponse(
            status_code=status.HTTP_200_OK,
            content={
                "token": token
            },
        )
    except Exception as e:
        await db.rollback()
        logging.warning(f"Database error for: {validated_data.username} - {e}")
        return ORJSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"detail": "Erreur de la base de données."},
        )