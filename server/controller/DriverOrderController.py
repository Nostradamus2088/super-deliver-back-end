import io
from typing import List
from utility.util import (
    HERE_API_KEY,
    STATE_OF_DEPLOYMENT,
    AsyncSession,
    DEFAULT_DATETIME,
    DEFAULT_IMAGE_FILENAME,
)
from fastapi.responses import ORJSONResponse
from sqlalchemy.future import select
from sqlalchemy.sql import func, text
from datetime import datetime, timedelta
from sqlalchemy import select, func, and_, delete
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status
from datetime import datetime, timedelta
from fastapi import status, HTTPException
import logging
import asyncio
import orjson
from datetime import datetime
from zoneinfo import ZoneInfo
from zoneinfo import ZoneInfo
import aiohttp
import json
from model.superDeliver.TentativeModel import (DriverTentativeOrder)
from model.superDeliver import (DriverOrderModel)
from model.LoggingModel import (SkippedPart)
from model.superDeliver.OrderModel import (Order)
from model.UpcModel import (Upc)
from model.superDeliver.DriverModel import (
    Driver, 
    DriverId, 
    DriverIdRoute, 
    StoreId
)
from model.superDeliver.DriverOrderModel import (
    DriverOrder,
    DriverOrderNumber,
    OrderReorderRequest,
    ScannedBatch,
    ScannedPart,
    InvoiceCode,
    InvoiceInfo,
)

def convert_minutes_to_hours(minutes):
    if minutes is None:
        return None
    hours = int(minutes // 60)
    remaining_minutes = int(minutes % 60)
    return f"{hours}h {remaining_minutes}min"


async def get_deliveries_time(store_id: int, driver_id: int, db: AsyncSession) -> ORJSONResponse:
    try:
        # Fetch the latest order's creation date for the specified driver and store
        latest_order_query = select(func.max(DriverOrder.created_at)).where(
            DriverOrder.driver_id == driver_id
        )
        if store_id is not None:
            latest_order_query = latest_order_query.where(DriverOrder.store == store_id)

        latest_order_result = await db.execute(latest_order_query)
        latest_created_at = latest_order_result.scalar()
        if not latest_created_at:
            return ORJSONResponse(
                status_code=404,
                content={"detail": "No recent orders found for the given parameters"},
            )

        last_30_days = latest_created_at - timedelta(days=30)
        last_7_days = latest_created_at - timedelta(days=7)

        async def calculate_average_delivery_time(start_time, consider_all_orders=False):
            query = select(
                func.avg(
                    func.timestampdiff(
                        text("MINUTE"), DriverOrder.created_at, DriverOrder.delivered_at
                    )
                ),
                func.count(),
            ).where(
                (DriverOrder.created_at >= start_time)
                & (DriverOrder.created_at <= latest_created_at)
                & (DriverOrder.delivered_at != '1000-01-01 12:00:00')
                & (DriverOrder.is_delivered == True)
            )

            if not consider_all_orders:
                query = query.where(DriverOrder.driver_id == driver_id)

            if store_id is not None:
                query = query.where(DriverOrder.store == store_id)

            results = await db.execute(query)
            avg_delivery_time, count = results.first()
            return avg_delivery_time, count

        # Calculating average delivery times and counts
        avg_30_days, count_30_days = await calculate_average_delivery_time(last_30_days)
        avg_7_days, count_7_days = await calculate_average_delivery_time(last_7_days)
        avg_overall, count_overall = await calculate_average_delivery_time(
            latest_created_at - timedelta(days=365)
        )  # Adjust this duration for overall average

        # Calculate total order count for the store without filtering by driver
        _, count_store_overall = await calculate_average_delivery_time(
            latest_created_at - timedelta(days=365), consider_all_orders=True
        )

        delivery_times = {
            "last_30_days": convert_minutes_to_hours(avg_30_days),
            "last_7_days": convert_minutes_to_hours(avg_7_days),
            "overall": convert_minutes_to_hours(avg_overall),
            "count_last_30_days": count_30_days,
            "count_last_7_days": count_7_days,
            "count_overall": count_store_overall,  # This now reflects the total store orders
        }

        return ORJSONResponse(status_code=200, content=delivery_times)
    except Exception as e:
        return ORJSONResponse(status_code=500, content={"detail": str(e)})

async def get_all_orders(
    validated_data: StoreId, db: AsyncSession, db_secondary: AsyncSession
) -> ORJSONResponse:
    try:
        # Fetch orders
        result_orders = await db.execute(
            select(DriverOrder, Driver.username)
            .join(Driver, DriverOrder.driver_id == Driver.id)
            .where(DriverOrder.store == validated_data.store)
             .order_by(DriverOrder.created_at.desc())
        )
        orders = result_orders.all()

        # Fetch tentative_orders
        result_tentative_orders = await db.execute(
            select(DriverTentativeOrder, Driver.username)
            .join(Driver, DriverTentativeOrder.driver_id == Driver.id)
            .where(DriverTentativeOrder.store == validated_data.store)
            .order_by(DriverTentativeOrder.order_number.asc())
        )
        tentative_orders = result_tentative_orders.all()

        # Prepare list of order numbers
        order_numbers = [order.order_number for order, _ in orders]
        tentative_order_numbers = [tent_order.order_number for tent_order, _ in tentative_orders]

        # Secondary database query for orders
        if order_numbers:
            result_secondary_orders = await db_secondary.execute(
                select(Order.pickers, Order.dispatch, Order.order_number, Order.created_at, Order.updated_at).where(
                    Order.order_number.in_(order_numbers)
                )
            )
            rows_orders = result_secondary_orders.fetchall()
            secondary_info_orders = {
                row.order_number: {
                    "pickers": row.pickers,
                    "dispatch": row.dispatch,
                    "dispatched_at": row.created_at,
                    "updated_at": row.updated_at,
                } for row in rows_orders
            }
        else:
            secondary_info_orders = {}

        # Secondary database query for tentative_orders
        if tentative_order_numbers:
            result_secondary_tentative_orders = await db_secondary.execute(
                select(Order.pickers, Order.dispatch, Order.order_number, Order.created_at, Order.updated_at).where(
                    Order.order_number.in_(tentative_order_numbers)
                )
            )
            rows_tentative_orders = result_secondary_tentative_orders.fetchall()
            secondary_info_tentative_orders = {
                row.order_number: {
                    "pickers": row.pickers,
                    "dispatch": row.dispatch,
                    "dispatched_at": row.created_at,
                    "updated_at": row.updated_at,
                } for row in rows_tentative_orders
            }
        else:
            secondary_info_tentative_orders = {}

        BASE_URL = {
            "prod": "https://deliver.pasuper.xyz",
            "dev": "https://deliver-dev.pasuper.xyz",
            "local": "http://127.0.0.1:8000",
        }

        image_url_env = BASE_URL.get(STATE_OF_DEPLOYMENT)

        # Process orders data
        orders_data = []
        for order, driver_username in orders:
            additional_data = secondary_info_orders.get(order.order_number, {
                "pickers": None, 
                "dispatch": None, 
                "dispatched_at": None, 
                "updated_at": None
            })
            
            if additional_data["dispatched_at"] and additional_data["updated_at"]:
                # Calculate avg picking time in minutes and seconds
                picking_time_seconds = (additional_data["updated_at"] - additional_data["dispatched_at"]).total_seconds()
                picking_minutes, picking_seconds = divmod(picking_time_seconds, 60)

                # Calculate avg dispatch time in minutes and seconds
                dispatch_time_seconds = (order.created_at - additional_data["updated_at"]).total_seconds()
                dispatch_minutes, dispatch_seconds = divmod(dispatch_time_seconds, 60)
            else:
                picking_minutes = picking_seconds = dispatch_minutes = dispatch_seconds = 0

            replacements = ["/home/images-dev", "/home/images", "home/images-dev", "home/images"]
            for path in replacements:
                order.signature_filename = order.signature_filename.replace(path, "images")
                order.photo_filename = order.photo_filename.replace(path, "images")

            order_dict = {
                "client_name": order.client_name,
                "order_number": order.order_number,
                "customer": order.customer,
                "phone_number": order.phone_number,
                "address": order.address,
                "created_at": order.created_at.strftime("%Y-%m-%d %H:%M:%S"),
                "arrived_at": order.arrived_at.strftime("%Y-%m-%d %H:%M:%S"),
                "delivered_at": order.delivered_at.strftime("%Y-%m-%d %H:%M:%S"),
                "updated_at": order.updated_at.strftime("%Y-%m-%d %H:%M:%S"),
                "order_info": order.order_info,
                "order_index": order.order_index,
                "driver_id": order.driver_id,
                "driver_username": driver_username,
                "driver_name": driver_username,
                "is_delivered": order.is_delivered,
                "pickers": additional_data["pickers"],
                "dispatch": additional_data["dispatch"],
                "avg_picking_time": f"{int(picking_minutes)} minutes {int(picking_seconds)} seconds",
                "avg_dispatch_time": f"{int(dispatch_minutes)} minutes {int(dispatch_seconds)} seconds",
                "signature_filename": f"{image_url_env}/{order.signature_filename}",
                "photo_filename": f"{image_url_env}/{order.photo_filename}",
                "route": order.route,
                "latitude": order.latitude,
                "longitude": order.longitude,
                "store": order.store,
                "route_started": order.route_started,
                "received_by": order.received_by,
                "dispatched_at": additional_data["dispatched_at"].strftime("%Y-%m-%d %H:%M:%S") if additional_data["dispatched_at"] else None,
            }
            orders_data.append(order_dict)

        # Process tentative orders data
        tentative_orders_data = []
        for order, driver_username in tentative_orders:
            additional_data = secondary_info_tentative_orders.get(order.order_number, {
                "pickers": None, 
                "dispatch": None, 
                "dispatched_at": None, 
                "updated_at": None
            })
            
            if additional_data["dispatched_at"] and additional_data["updated_at"]:
                # Calculate avg picking time in minutes and seconds
                picking_time_seconds = (additional_data["updated_at"] - additional_data["dispatched_at"]).total_seconds()
                picking_minutes, picking_seconds = divmod(picking_time_seconds, 60)
            else:
                picking_minutes = picking_seconds = 0


            tentative_order_dict = {
                "client_name": order.client_name,
                "order_number": order.order_number,
                "customer": order.customer,
                "phone_number": order.phone_number,
                "address": order.address,
                "created_at": order.created_at.strftime("%Y-%m-%d %H:%M:%S"),
                "tentative_at": order.tentative_at.strftime("%Y-%m-%d %H:%M:%S"),
                "updated_at": order.updated_at.strftime("%Y-%m-%d %H:%M:%S"),
                "order_info": order.order_info,
                "driver_id": order.driver_id,
                "driver_username": driver_username,
                "driver_name": driver_username,
                "avg_picking_time": f"{int(picking_minutes)} minutes {int(picking_seconds)} seconds",
                "route": order.route, 
                "route_started": order.route_started,
                "received_by": order.received_by,
                "dispatched_at": additional_data["dispatched_at"].strftime("%Y-%m-%d %H:%M:%S") if additional_data["dispatched_at"] else None,
            }
            tentative_orders_data.append(tentative_order_dict)

        # Combine both arrays in the response
        response_data = {
            "orders": orders_data,
            "tentative_orders": tentative_orders_data,
        }

        return ORJSONResponse(status_code=status.HTTP_200_OK, content=response_data)

    except Exception as e:
        logging.error(f"Error fetching all orders: {e}")
        return ORJSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"detail": "Unknown error occurred while retrieving orders."},
        )
    
async def get_all_routes_by_driverId(validated_data: DriverIdRoute, db: AsyncSession) -> ORJSONResponse:
    try:
        # Fetch all orders by driver_id and route number
        result = await db.execute(
            select(DriverOrder)
            .where(
                DriverOrder.driver_id == validated_data.driver_id,
                DriverOrder.route == validated_data.route_number
            )
        )
        orders = result.scalars().all()

        if not orders:
            return ORJSONResponse(status_code=status.HTTP_200_OK, content=[])

        orders_data = [
            {
                "id": order.id,
                "order_number": order.order_number,
                "store": order.store,
                "customer": order.customer,
                "order_info": order.order_info,
                "client_name": order.client_name,
                "phone_number": order.phone_number,
                "latitude": order.latitude,
                "longitude": order.longitude,
                "address": order.address,
                "ship_addr": order.ship_addr,
                "driver_id": order.driver_id,
                "signature_filename": order.signature_filename,
                "photo_filename": order.photo_filename,
                "is_arrived": order.is_arrived,
                "is_delivered": order.is_delivered,
                "created_at": order.created_at.strftime("%Y-%m-%d %H:%M:%S"),
                "arrived_at": order.arrived_at.strftime("%Y-%m-%d %H:%M:%S") if order.arrived_at else None,
                "delivered_at": order.delivered_at.strftime("%Y-%m-%d %H:%M:%S") if order.delivered_at else None,
                "updated_at": order.updated_at.strftime("%Y-%m-%d %H:%M:%S"),
                "driver_name": order.driver_name,
                "order_index": order.order_index,
                "route": order.route,
                "route_started": order.route_started,
                "received_by": order.received_by,
            }
            for order in orders
        ]

        # Add the average delivery time to the response
        response_data = {
            "orders": orders_data,
        }

        return ORJSONResponse(
            status_code=status.HTTP_200_OK,
            content=response_data,
        )

    except Exception as e:
        logging.error(f"Error fetching all orders by route: {e}")
        return ORJSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"detail": "Unknown error occurred while retrieving orders by route."},
        )
    
async def get_all_orders_by_id(validated_data: DriverId, db: AsyncSession) -> ORJSONResponse:
    try:
        # Fetch all orders by driver_id
        result = await db.execute(
            select(DriverOrder)
            .where(
                DriverOrder.driver_id == validated_data.driver_id,
            )
            .order_by(DriverOrder.order_number.asc())
        )
        orders = result.scalars().all()

        if not orders:
            return ORJSONResponse(status_code=status.HTTP_200_OK, content=[])

        orders_data = [
            {
                "client_name": order.client_name,
                "order_number": order.order_number,
                "customer": order.customer,
                "phone_number": order.phone_number,
                "address": order.address,
                "latitude": order.latitude,
                "longitude": order.longitude,
                "is_delivered": order.is_delivered,
                "created_at": order.created_at.strftime("%Y-%m-%d %H:%M:%S"),
                "arrived_at": order.arrived_at.strftime("%Y-%m-%d %H:%M:%S") if order.arrived_at else None,
                "delivered_at": order.delivered_at.strftime("%Y-%m-%d %H:%M:%S") if order.delivered_at else None,
                "updated_at": order.updated_at.strftime("%Y-%m-%d %H:%M:%S"),
                "order_info": order.order_info,
                "driver_id": order.driver_id,
                "route": order.route, 
                "route_started": order.route_started,
                "received_by": order.received_by,
            }
            for order in orders
        ]

        # Calculate the average overall delivery time for all orders
        avg_delivery_time_query = select(
            func.avg(
                func.timestampdiff(
                    text("MINUTE"), DriverOrder.created_at, DriverOrder.delivered_at
                )
            ).label("avg_delivery_time")
        ).where(
            DriverOrder.driver_id == validated_data.driver_id,
            DriverOrder.delivered_at != '1000-01-01 12:00:00',
            DriverOrder.is_delivered == True
        )

        avg_delivery_time_result = await db.execute(avg_delivery_time_query)
        avg_delivery_time = avg_delivery_time_result.scalar()

        # Add the average delivery time to the response
        response_data = {
            "orders": orders_data,
            "average_delivery_time_hours": convert_minutes_to_hours(avg_delivery_time)
        }

        return ORJSONResponse(
            status_code=status.HTTP_200_OK,
            content=response_data,
        )

    except Exception as e:
        logging.error(f"Error fetching all orders: {e}")
        return ORJSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"detail": "Unknown error occurred while retrieving orders."},
        )

async def get_order_by_number(
    validated_data: DriverOrderNumber, db_primary: AsyncSession, db_secondary: AsyncSession
) -> ORJSONResponse:
    try:
        # Fetch the DriverOrder and related Order data
        driver_order_result = await db_primary.execute(
            select(DriverOrder, Driver.username)
            .join(Driver, DriverOrder.driver_id == Driver.id)
            .where(DriverOrder.order_number == validated_data.order_number)
            .limit(1)
        )
        driver_order_result = driver_order_result.one_or_none()

        if not driver_order_result:
            return ORJSONResponse(status_code=status.HTTP_404_NOT_FOUND, content={"detail": "Order not found"})

        driver_order, driver_username = driver_order_result

        order_result = await db_secondary.execute(
            select(Order)
            .where(Order.order_number == validated_data.order_number)
            .limit(1)
        )
        order_result = order_result.one_or_none()

        if not order_result:
            return ORJSONResponse(status_code=status.HTTP_404_NOT_FOUND, content={"detail": "Order not found"})

        order = order_result[0]

        # Calculate avg picking time in minutes and seconds
        picking_time_seconds = (order.updated_at - order.created_at).total_seconds()
        picking_minutes, picking_seconds = divmod(picking_time_seconds, 60)

        # Calculate avg dispatch time in minutes and seconds
        dispatch_time_seconds = (driver_order.created_at - order.updated_at).total_seconds()
        dispatch_minutes, dispatch_seconds = divmod(dispatch_time_seconds, 60)

        order_data = {
            "client_name": driver_order.client_name,
            "order_number": driver_order.order_number,
            "customer": driver_order.customer,
            "phone_number": driver_order.phone_number,
            "address": driver_order.address,
            "created_at": driver_order.created_at.strftime("%Y-%m-%d %H:%M:%S"),
            "arrived_at": driver_order.arrived_at.strftime("%Y-%m-%d %H:%M:%S"),
            "delivered_at": driver_order.delivered_at.strftime("%Y-%m-%d %H:%M:%S"),
            "updated_at": order.updated_at.strftime("%Y-%m-%d %H:%M:%S"),
            "order_info": driver_order.order_info,
            "driver_id": driver_order.driver_id,
            "driver_name": driver_username,
            "avg_picking_time": f"{int(picking_minutes)} minutes {int(picking_seconds)} seconds",
            "avg_dispatch_time": f"{int(dispatch_minutes)} minutes {int(dispatch_seconds)} seconds",
            "route":driver_order.route,
            "received_by": driver_order.received_by
        }

        return ORJSONResponse(status_code=status.HTTP_200_OK, content=order_data)

    except Exception as e:
        print(f"Error: {e}")
        return ORJSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"detail": "Unknown error occurred while retrieving the order."}
        )

async def get_driver_orders(
    driver_id: int,
    db: AsyncSession,
) -> ORJSONResponse:
    try:
        result = await db.execute(
            select(DriverOrder)
            .where(
                and_(
                    DriverOrder.driver_id == driver_id,
                    DriverOrder.is_delivered == 0,
                )
            )
            .order_by(DriverOrder.order_index.asc())
        )

        driver_orders = result.scalars().all()

        if not driver_orders:
            return ORJSONResponse(status_code=status.HTTP_200_OK, content=[])

        orders_data = [
            {
                "order_number": order.order_number,
                "store": order.store,
                "customer": order.customer,
                "order_info": order.order_info,
                "client_name": order.client_name,
                "phone_number": order.phone_number,
                "address": order.address,
                "driver_id": order.driver_id,
                "is_arrived": order.is_arrived,
                "latitude": order.latitude,
                "longitude": order.longitude,
                "order_index": order.order_index,
                "route": order.route, 
                "route_started": order.route_started,
                "received_by": order.received_by,
            }
            for order in driver_orders
        ]

        return ORJSONResponse(
            status_code=status.HTTP_200_OK,
            content=orders_data,
        )

    except ORJSONResponse as e:
        logging.error(f"Error fetching driver orders: {e}")
        return ORJSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={
                "detail": "Erreur inconue lors de la récupération des commandes du livreur."
            },
        )


async def reorder_driver_orders(
        driver_id: str,
            validated_data: List[OrderReorderRequest],
            db: AsyncSession
        ) -> ORJSONResponse:
        try:
            logging.info(f"Received data: {validated_data} for driver_id: {driver_id}")

            # Fetch the existing orders for the specific driver from the database
            result = await db.execute(
                select(DriverOrderModel.DriverOrder).filter_by(driver_id=driver_id).order_by(DriverOrderModel.DriverOrder.order_index.asc())
            )
            existing_orders = result.scalars().all()

            existing_order_dict = {order.order_number: order for order in existing_orders}
            new_index_mapping = {order_request.order_number: order_request.new_index for order_request in validated_data}

            for order_number, new_index in new_index_mapping.items():
                if order_number in existing_order_dict:
                    existing_order_dict[order_number].order_index = new_index

            # Filter out orders with NoneType order_index and sort the remaining orders
            valid_orders = [order for order in existing_orders if order.order_index is not None]
            sorted_orders = sorted(valid_orders, key=lambda order: order.order_index)

            # Ensure order_index is sequential and starts from 1
            for index, order in enumerate(sorted_orders, start=1):
                order.order_index = index

            await db.commit()
            return ORJSONResponse({"detail": "Order updated successfully."})

        except Exception as e:
            logging.error(f"Error reordering driver orders: {e}")
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                detail=f"Error reordering driver orders: {e}"
        )
    



async def set_driver_order(
    driver_id: int,
    validated_data: InvoiceCode,
    db: AsyncSession,
    db_sec: AsyncSession,
    route: str,
    received_by: str
) -> ORJSONResponse:
    """
    Assign an order to a driver after fetching and validating order details.

    Parameters:
        driver_id (int): The ID of the driver.
        validated_data (InvoiceCode): The validated order data.
        db (AsyncSession): The primary database session.
        db_sec (AsyncSession): The secondary database session.

    Returns:
        ORJSONResponse: The HTTP response.
    """
    try:
        # Fetch the order from the secondary database
        order_query = (
            select(Order)
            .where(
                and_(
                    Order.order_number == validated_data.order_number,
                    Order.store == validated_data.store,
                )
            )
            .limit(1)
        )
        order_result = await db_sec.execute(order_query)
        order = order_result.scalar_one_or_none()

        if order is None:
            return ORJSONResponse(
                content={"detail": "Commandes non trouvées."},
                status_code=status.HTTP_404_NOT_FOUND,
            )

        if not order.order_info:
            return ORJSONResponse(
                content={"detail": "Il n'y a pas de produits dans la commande."},
                status_code=status.HTTP_404_NOT_FOUND,
            )

        # Check if the order has already been processed
        existing_order_query = (
            select(DriverOrder)
            .where(
                and_(
                    DriverOrder.order_number == validated_data.order_number,
                    DriverOrder.store == validated_data.store,
                )
            )
            .limit(1)
        )
        existing_order_result = await db.execute(existing_order_query)
        existing_order = existing_order_result.scalar_one_or_none()

        if existing_order:
            return ORJSONResponse(
                content={"detail": "La commande a déjà été traitée."},
                status_code=status.HTTP_400_BAD_REQUEST,
            )

        # Check if all items in order_info are processed once
        try:
            order_info = json.loads(order.order_info)
            seen_items = set()
            for item in order_info:
                if item['item'] in seen_items:
                    return ORJSONResponse(
                        content={"detail": f"Item {item['item']} is duplicated."},
                        status_code=status.HTTP_400_BAD_REQUEST,
                    )
                seen_items.add(item['item'])
                if item['units'] <= 0:
                    return ORJSONResponse(
                        content={"detail": f"Item {item['item']} has invalid units."},
                        status_code=status.HTTP_400_BAD_REQUEST,
                    )
        except (json.JSONDecodeError, KeyError, ValueError) as e:
            return ORJSONResponse(
                content={"detail": "Invalid order_info data."},
                status_code=status.HTTP_400_BAD_REQUEST,
            )

        # Convert store to integer
        order.store = int(order.store)

        # Fetch driver username
        driver_username_query = (
            select(Driver.username)
            .where(Driver.id == driver_id)
            .limit(1)
        )
        driver_username_result = await db.execute(driver_username_query)
        driver_username = driver_username_result.scalar_one_or_none()

        if not driver_username:
            return ORJSONResponse(
                content={"detail": "Livreur non trouvé."},
                status_code=status.HTTP_404_NOT_FOUND,
            )

        # Check if the order exists in tentative_orders
        tentative_order_query = (
            select(DriverTentativeOrder)
            .where(
                and_(
                    DriverTentativeOrder.order_number == order.order_number,
                    DriverTentativeOrder.store == order.store,
                )
            )
            .limit(1)
        )

        tentative_order_result = await db.execute(tentative_order_query)
        tentative_order = tentative_order_result.scalar_one_or_none()

        if tentative_order:
            # Delete the order from tentative_orders
            delete_query = delete(DriverTentativeOrder).where(
                and_(
                    DriverTentativeOrder.order_number == order.order_number,
                    DriverTentativeOrder.store == order.store,
                )
            )
            await db.execute(delete_query)

            # Prepare data for new order in drivers_orders
            new_order_data = tentative_order
            address = tentative_order.address
            latitude = tentative_order.latitude
            longitude = tentative_order.longitude
        else:
            # Prepare data for new order from fetched order
            new_order_data = order
            address, latitude, longitude = await fetch_clean_addresses(
                order.address1, order.address2, order.address3,
                order.ship_addr1, order.ship_addr2, order.ship_addr3
            )

        if latitude is None or longitude is None:
            return ORJSONResponse(
                content={"detail": "Unable to fetch valid coordinates."},
                status_code=status.HTTP_400_BAD_REQUEST,
            )

        # Process order_info JSON
        new_order_info = process_order_info(new_order_data.order_info)

        # Get the current time in Eastern Time
        eastern = ZoneInfo('America/New_York')
        created_at = datetime.now(eastern)

        # Fetch existing orders for the same client
        existing_orders = await fetch_existing_orders_for_driver(new_order_data.client_name,route, db)

        # Filter orders that are not arrived and not delivered
        existing_orders = [order for order in existing_orders if not order.is_arrived and not order.is_delivered]

        # if existing_orders:
        #     # Determine the smallest order_index among the existing orders
        #     existing_order_indexes = [order.order_index for order in existing_orders]
        #     smallest_order_index = min(existing_order_indexes)
            
        #     new_order_info = merge_order_info(new_order_info, existing_orders)
        #     await delete_existing_orders(existing_orders, db)
        # else:
            # Fetch the closest order_index for the driver and increment by 1 if no merge
        closest_order_index_query = (
            select(DriverOrder.order_index)
            .where(DriverOrder.driver_id == driver_id)
            .order_by(DriverOrder.order_index.desc())
            .limit(1)
        )
        closest_order_index_result = await db.execute(closest_order_index_query)
        smallest_order_index = closest_order_index_result.scalar_one_or_none() or 0
        smallest_order_index += 1

        # Check if the driver has existing undelivered orders
        existing_driver_orders_query = (
            select(DriverOrder)
            .where(DriverOrder.route == route)
        )
        existing_driver_orders_result = await db.execute(existing_driver_orders_query)
        existing_driver_orders = existing_driver_orders_result.scalars().all()

        undelivered_orders = [order for order in existing_driver_orders if not order.is_delivered]

        # Increment route only if this is the first undelivered order for the driver
        if not undelivered_orders:
            # Get the current route number
            route_query = (
                select(DriverOrder.route)
                .where(DriverOrder.driver_id == driver_id)
                .order_by(DriverOrder.route.desc())
                .limit(1)
            )
            route_result = await db.execute(route_query)
            current_route = route_result.scalar_one_or_none()

            # If there are no orders yet, start with route 1
            new_route_number = 1 if current_route is None else int(current_route.split("-")[1]) + 1
        else:
            # Use the current route without incrementing
            current_route = undelivered_orders[0].route
            new_route_number = int(current_route.split("-")[1])

        # Create new route string with driver ID
        new_route = f"{driver_id}-{new_route_number}"

        # Create new driver order
        new_driver_order = create_new_driver_order(
            driver_id, driver_username, new_order_data, address, latitude, longitude, new_order_info, created_at, smallest_order_index, new_route, received_by,
        )

        # Ensure the route_started field is set properly
        new_driver_order.route_started = False

        db.add(new_driver_order)
        await db.commit()

        return ORJSONResponse(
            content={"detail": "La commande a été associée au livreur."},
            status_code=status.HTTP_200_OK,
        )
    except ValueError as ve:
        logging.error(f"Validation error: {str(ve)}")
        return ORJSONResponse(
            content={"detail": "Erreur de validation."},
            status_code=status.HTTP_400_BAD_REQUEST,
        )
    except Exception as e:
        logging.error(f"Unexpected error occurred: {str(e)}")
        await db.rollback()
        return ORJSONResponse(
            content={"detail": "Une erreur inconnue est survenue. Scanné 2 fois!"},
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )
    
async def get_routes_order(
        driver_id: int,
        db: AsyncSession
    ) -> ORJSONResponse:
        try:
            route_query = (
                select(DriverOrder.route)
                .where(DriverOrder.driver_id == driver_id)
                .order_by(DriverOrder.route.desc())
                .limit(1)
            )
            route_result = await db.execute(route_query)
            current_route = route_result.scalar_one_or_none()

            # If there are no orders yet, start with route 1
            route = 1 if current_route is None else current_route

            return ORJSONResponse(
                content={"route": route},
                status_code=status.HTTP_200_OK,
            )
        except Exception as e:
            logging.error(f"Unexpected error occurred: {str(e)}")
            return ORJSONResponse(
                content={"detail": "An error occurred while fetching the route number."},
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )    
    
async def fetch_clean_addresses(
    address1: str,
    address2: str,
    address3: str,
    ship_addr1: str,
    ship_addr2: str,
    ship_addr3: str,
) -> tuple[str, float, float]:
    """
    Fetch clean address and coordinates using HERE API.

    Parameters:
        address1, address2, address3 (str): Primary addresses.
        ship_addr1, ship_addr2, ship_addr3 (str): Shipping addresses.

    Returns:
        tuple[str, float, float]: Clean address, latitude, and longitude.
    """
    def create_query_address(addr1, addr2, addr3):
        return f"{addr1}, {addr2}, {addr3}"

    def contains_psl(*args):
        return any("PSL" in addr for addr in args if addr)

    def extract_address_with_postal_code(*args):
        for addr in args:
            if addr and any(char.isdigit() for char in addr):
                return addr
        return "Unknown address"

    if contains_psl(address1, address2, address3):
        if contains_psl(ship_addr1, ship_addr2, ship_addr3):
            query_address = extract_address_with_postal_code(
                address1, address2, address3, ship_addr1, ship_addr2, ship_addr3
            ) or create_query_address(ship_addr1, ship_addr2, ship_addr3)
        else:
            query_address = create_query_address(ship_addr1, ship_addr2, ship_addr3)
    else:
        if contains_psl(ship_addr1, ship_addr2, ship_addr3):
            query_address = create_query_address(address1, address2, address3)
        else:
            query_address = create_query_address(address1, address2, address3)

    base_url = "https://geocode.search.hereapi.com/v1/discover"
    params = {
        "q": query_address,
        "apiKey": HERE_API_KEY,
        "in": "bbox:-79.7624,45.0118,-57.1103,63.0001",
    }

    async with aiohttp.ClientSession() as session:
        async with session.get(base_url, params=params) as response:
            if response.status == 200:
                results = await response.json()
                items = results.get("items", [])
                if items:
                    primary_result = items[0]
                    clean_address = primary_result.get("title")
                    position = primary_result.get("position")
                    latitude = position.get("lat")
                    longitude = position.get("lng")
                    return clean_address, latitude, longitude

    return "Unknown address", 0.0, 0.0

async def fetch_existing_orders_for_driver(client_name: str, route: str, db: AsyncSession):
    """
    Fetch existing orders for the same client and route.
    Parameters:
        client_name (str): The name of the client.
        route (str): The route identifier.
        db (AsyncSession): The primary database session.
    Returns:
        List[DriverOrder]: List of existing orders.
    """
    try:
        existing_orders_query = (
            select(DriverOrder)
            .where(
                and_(
                    DriverOrder.client_name == client_name,
                    DriverOrder.route == route,
                )
            )
        )
        existing_orders_result = await db.execute(existing_orders_query)
        existing_orders = existing_orders_result.scalars().all()
        return existing_orders
    except Exception as e:
        print(f"Error fetching existing orders: {e}")
        return []

def merge_order_info(new_order_info: str, existing_orders: list) -> str:
    """
    Merge order info with existing orders.

    Parameters:
        new_order_info (str): The new order information.
        existing_orders (list): List of existing orders.

    Returns:
        str: Merged order information.
    """
    all_items = orjson.loads(new_order_info)
    merged_items = {}

    for item in all_items:
        key = item['item']
        if key in merged_items:
            merged_items[key]['units'] += item['units']
        else:
            merged_items[key] = item

    for order in existing_orders:
        items = orjson.loads(order.order_info)
        for item in items:
            key = item['item']
            if key in merged_items:
                merged_items[key]['units'] += item['units']
            else:
                merged_items[key] = item

    merged_items_list = list(merged_items.values())
    return orjson.dumps(merged_items_list).decode("utf-8")

async def delete_existing_orders(existing_orders: list, db: AsyncSession):
    """
    Delete existing orders.

    Parameters:
        existing_orders (list): List of existing orders.
        db (AsyncSession): The database session.
    """
    for order in existing_orders:
        await db.delete(order)

def process_order_info(order_info: str) -> str:
    """
    Process order information JSON.

    Parameters:
        order_info (str): The order information JSON.

    Returns:
        str: Processed order information JSON.
    """
    new_order_info = []
    for item in orjson.loads(order_info):
        new_order_info.append(
            {
                "item": item["item"],
                "description": item["description"],
                "units": int(item["units"]),
                "num_scanned": int(0),
                "confirmed_scanned": int(0),
            }
        )
    return orjson.dumps(new_order_info).decode("utf-8")

def create_new_driver_order(
    driver_id: int, driver_username: str, new_order_data, address, latitude, longitude, new_order_info, created_at, new_order_index, route: int, received_by:str
) -> DriverOrder:
    """
    Create a new driver order.

    Parameters:
        driver_id (int): The ID of the driver.
        driver_username (str): The username of the driver.
        new_order_data: The new order data.
        address: The address of the order.
        latitude: The latitude of the order location.
        longitude: The longitude of the order location.
        new_order_info (str): The new order information.
        created_at: The creation time of the order.
        new_order_index: The new order index.

    Returns:
        DriverOrder: The new driver order.
    """
    return DriverOrder(
        order_number=new_order_data.order_number,
        store=new_order_data.store,
        customer=new_order_data.customer,
        order_info=new_order_info,
        client_name=new_order_data.client_name,
        phone_number=new_order_data.phone_number,
        driver_id=driver_id,
        driver_name=driver_username,  # Add the driver's username to driver_name column
        latitude=latitude,
        longitude=longitude,
        address=address,
        ship_addr=address,
        signature_filename=DEFAULT_IMAGE_FILENAME,
        photo_filename=DEFAULT_IMAGE_FILENAME,
        is_arrived=False,
        is_delivered=False,
        created_at=created_at,
        arrived_at=DEFAULT_DATETIME,
        delivered_at=DEFAULT_DATETIME,
        order_index=new_order_index,
        route = route,
        received_by = received_by
    )


async def set_to_arrived(
    validated_data: InvoiceInfo,
    db: AsyncSession,
) -> ORJSONResponse:
    try:
        result = await db.execute(
            select(DriverOrder)
            .where(
                and_(
                    DriverOrder.order_number == validated_data.orderNumber,
                    DriverOrder.store == validated_data.store,
                )
            )
            .limit(1)
        )

        driver_order = result.scalars().first()

        if not driver_order:
            return ORJSONResponse(
                status_code=status.HTTP_404_NOT_FOUND,
                content={"detail": "Commande non trouvée."},
            )

        # Get the current time in Eastern Time
        eastern = ZoneInfo('America/New_York')
        created_at = datetime.now(eastern)

        driver_order.is_arrived = True
        driver_order.arrived_at = created_at

        await db.commit()
        await db.refresh(driver_order)

        return ORJSONResponse(
            status_code=status.HTTP_200_OK,
            content={"detail": "Commande marquée comme arrivé."},
        )

    except Exception as e:
        logging.error(f"Error fetching driver orders: {e}")
        return ORJSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"detail": "Erreur inconue lors de la requête."},
        )

async def set_to_delivered(
    validated_data: InvoiceInfo,
    db: AsyncSession,
) -> ORJSONResponse:
    try:
        result = await db.execute(
            select(DriverOrder)
            .where(
                and_(
                    DriverOrder.order_number == validated_data.orderNumber,
                    DriverOrder.store == validated_data.store,
                )
            )
            .limit(1)
        )

        driver_order = result.scalars().first()

        if not driver_order:
            return ORJSONResponse(
                status_code=status.HTTP_404_NOT_FOUND,
                content={"detail": "Commande non trouvée."},
            )

        if driver_order.photo_filename == DEFAULT_IMAGE_FILENAME and not driver_order.received_by:
            return ORJSONResponse(
                status_code=status.HTTP_400_BAD_REQUEST,
                content={"detail": "Photo ou réception par une personne manquante."},
            )


        order_info = orjson.loads(driver_order.order_info)
        for item in order_info:
            if item["confirmed_scanned"] < item["units"]:
                return ORJSONResponse(
                    content={
                        "detail": "Impossible de marquée comme livrée, car l'ensemble des pièces n'a pas été scanné une deuxième fois."
                    },
                    status_code=status.HTTP_409_CONFLICT,
                )

        if driver_order.is_arrived is False:
            logging.warning(
                "Driver is not set to arrived but all parts where confirmed. This should not happen."
            )
            return ORJSONResponse(
                status_code=status.HTTP_409_CONFLICT,
                content={"detail": "Commande n'est pas marqué comme arrivé."},
            )

        # Get the current time in Eastern Time
        eastern = ZoneInfo('America/New_York')
        delivered_at = datetime.now(eastern)

        driver_order.is_delivered = True
        driver_order.delivered_at = delivered_at

        await db.commit()
        await db.refresh(driver_order)

        return ORJSONResponse(
            status_code=status.HTTP_200_OK,
            content={"detail": "Commande marquée comme livrée."},
        )

    except Exception as e:
        logging.error(f"Error fetching driver orders: {e}")
        return ORJSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"detail": "Erreur inconue lors de la requête."},
        )


async def remove_driver_order(
    validated_data: InvoiceInfo,
    db: AsyncSession,
    is_dispatch: bool = False,
) -> ORJSONResponse:
    try:
        delete_query = None

        # Prepare and execute the delete query
        if is_dispatch:
            delete_query = delete(DriverOrder).where(
                and_(
                    DriverOrder.order_number == validated_data.orderNumber,
                    DriverOrder.store == validated_data.store,
                )
            )
        else:
            delete_query = delete(DriverOrder).where(
                and_(
                    DriverOrder.order_number == validated_data.orderNumber,
                    DriverOrder.store == validated_data.store,
                    DriverOrder.is_delivered == 0,
                )
            )

        result = await db.execute(delete_query)

        # Check if any rows were affected
        if result.rowcount == 0:
            logging.warning("Order not found or insufficient privileges.")
            return ORJSONResponse(
                content={"detail": "Commande non trouvée."},
                status_code=status.HTTP_404_NOT_FOUND,
            )

        # Commit the transaction
        await db.commit()
        return ORJSONResponse(
            content={"detail": "Commande supprimée avec succès."},
            status_code=status.HTTP_200_OK,
        )
    except Exception as e:
        logging.error(f"Unexpected error occurred: {str(e)}")
        await db.rollback()
        return ORJSONResponse(
            content={
                "detail": "Erreur inconnue lors de la suppression de la commande."
            },
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )
    


async def retour_driver_orders(
    validated_data: InvoiceInfo,
    db: AsyncSession,
    is_dispatch: bool = False,
) -> ORJSONResponse:
    try:
        # Fetch the existing order's data
        fetch_query = select(DriverOrder).where(
            and_(
                DriverOrder.order_number == validated_data.orderNumber,
                DriverOrder.store == validated_data.store,
                DriverOrder.is_delivered == 0 if not is_dispatch else True
            )
        )
        result = await db.execute(fetch_query)
        existing_order = result.scalar_one_or_none()

        if not existing_order:
            logging.warning("Order not found or insufficient privileges.")
            return ORJSONResponse(
                content={"detail": "Commande non trouvée."},
                status_code=status.HTTP_404_NOT_FOUND,
            )

        # Prepare and execute the delete query
        delete_query = delete(DriverOrder).where(
            and_(
                DriverOrder.order_number == validated_data.orderNumber,
                DriverOrder.store == validated_data.store,
                DriverOrder.is_delivered == 0 if not is_dispatch else True
            )
        )
        await db.execute(delete_query)

        # Get current time in Eastern Time
        eastern = ZoneInfo('America/New_York')
        tentative_at = datetime.now(eastern)

        # Prepare the new entry for tentative_orders
        tentative_order = DriverTentativeOrder(
            order_number=existing_order.order_number,
            store=existing_order.store,
            customer=existing_order.customer,
            order_info=existing_order.order_info,
            client_name=existing_order.client_name,
            phone_number=existing_order.phone_number,
            latitude = existing_order.latitude,
            longitude = existing_order.longitude,
            address=existing_order.address,
            driver_id=existing_order.driver_id,
            created_at=existing_order.created_at,
            tentative_at=tentative_at,
            driver_name=existing_order.driver_name,
            route=existing_order.route,
            received_by=existing_order.received_by
        )

        db.add(tentative_order)
        await db.commit()

        return ORJSONResponse(
            content={"detail": "Commande retourner et réécrite avec succès."},
            status_code=status.HTTP_200_OK,
        )
    except Exception as e:
        logging.error(f"Unexpected error occurred: {str(e)}")
        await db.rollback()
        return ORJSONResponse(
            content={
                "detail": "Erreur inconnue lors de la suppression de la commande."
            },
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )


async def scan_part(
    validated_data: ScannedPart,
    db: AsyncSession,
    db_sec: AsyncSession,
    scan_type: str = "num_scanned",
) -> ORJSONResponse:
    try:
        upc_stmt = select(Upc).where(Upc.upc == validated_data.partCode).limit(1)

        driver_order_stmt = (
            select(DriverOrder)
            .where(
                and_(
                    DriverOrder.order_number == validated_data.orderNumber,
                    DriverOrder.store == validated_data.store,
                    DriverOrder.is_delivered == 0,
                )
            )
            .limit(1)
        )

        upc_result, driver_order_result = await asyncio.gather(
            db_sec.execute(upc_stmt),
            db.execute(driver_order_stmt),
        )

        driver_order = driver_order_result.scalar_one_or_none()
        if scan_type == "confirmed_scanned" and driver_order.is_arrived is False:
            return ORJSONResponse(
                content={
                    "detail": "Impossible de confirmer, car le livreur n'est pas marqué comme arrivé."
                },
                status_code=status.HTTP_409_CONFLICT,
            )

        if driver_order is None:
            logging.warning(
                f"Driver order not found for order number: {validated_data.orderNumber}"
            )
            return ORJSONResponse(
                content={"detail": "Commande non trouvée."},
                status_code=status.HTTP_404_NOT_FOUND,
            )

        upc = upc_result.scalar_one_or_none()
        if upc is None:
            logging.warning(f"Part upc not found: {validated_data.orderNumber}")
            return ORJSONResponse(
                content={"detail": "Upc non trouvé."},
                status_code=status.HTTP_404_NOT_FOUND,
            )

        order_info = orjson.loads(driver_order.order_info)
        for item in order_info:
            if item["item"] == upc.part_number:
                if (
                    scan_type == "confirmed_scanned"
                    and item["num_scanned"] < item["units"]
                ):
                    return ORJSONResponse(
                        content={
                            "detail": "Impossible de confirmer, car les pièces n'ont pas toute été scanné une première fois."
                        },
                        status_code=status.HTTP_409_CONFLICT,
                    )

                if item[scan_type] >= item["units"]:
                    return ORJSONResponse(
                        content={
                            "detail": f"Toutes les unités de {item["item"]} ont déjà été scannées."
                        },
                        status_code=status.HTTP_409_CONFLICT,
                    )

                item[scan_type] = item[scan_type] + 1
                driver_order.order_info = orjson.dumps(order_info).decode("utf-8")
                await db.commit()
                await db.refresh(driver_order)

                return ORJSONResponse(
                    content={
                        scan_type: item[scan_type],
                        "part_number": upc.part_number,
                    },
                    status_code=status.HTTP_200_OK,
                )

        logging.warning(
            f"Part number {upc.part_number} not found in order number: {validated_data.orderNumber}"
        )
        return ORJSONResponse(
            content={"detail": "Pièce non trouvée dans la commande."},
            status_code=status.HTTP_404_NOT_FOUND,
        )
    except Exception as error:
        await db.rollback()
        logging.exception(
            f"Exception occurred for order number {validated_data.orderNumber}: {error}"
        )
        return ORJSONResponse(
            content={"detail": "Erreur inconnue lors du scan de la pièce."},
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )


async def skip_part_at_delivery(
    driver_id: int,
    validated_data: ScannedBatch,
    db: AsyncSession,
) -> ORJSONResponse:
    try:
        stmt = (
            select(DriverOrder)
            .where(
                and_(
                    DriverOrder.order_number == validated_data.orderNumber,
                    DriverOrder.store == validated_data.store,
                    DriverOrder.is_delivered == 0,
                )
            )
            .limit(1)
        )
        result = await db.execute(stmt)
        driver_order = result.scalar_one_or_none()

        if driver_order is None:
            logging.warning(
                f"Driver order not found for order number: {validated_data.orderNumber}"
            )
            return ORJSONResponse(
                content={"detail": "Commande non trouvée."},
                status_code=status.HTTP_404_NOT_FOUND,
            )

        if driver_order.is_arrived is False:
            return ORJSONResponse(
                content={
                    "detail": "Impossible de confirmer, car le livreur n'est pas marqué comme arrivé."
                },
                status_code=status.HTTP_409_CONFLICT,
            )

        order_info = orjson.loads(driver_order.order_info)

        for item in order_info:
            if item["item"] == validated_data.partNumber:
                if item["confirmed_scanned"] >= item["units"]:
                    return ORJSONResponse(
                        content={
                            "detail": "Toutes les unités de la pièce ont déjà été confirmé."
                        },
                        status_code=status.HTTP_409_CONFLICT,
                    )

                skipped_part_record = SkippedPart(
                    driver_id=driver_id,
                    order_number=validated_data.orderNumber,
                    store=validated_data.store,
                    part_number=validated_data.partNumber,
                    created_at=datetime.utcnow(),
                )

                db.add(skipped_part_record)

                item["confirmed_scanned"] = item["units"]
                driver_order.order_info = orjson.dumps(order_info).decode("utf-8")
                await db.commit()
                await db.refresh(driver_order)

                return ORJSONResponse(
                    content={"confirmed_scanned": item["confirmed_scanned"]},
                    status_code=status.HTTP_200_OK,
                )

        logging.warning(
            f"Part number {validated_data.partNumber} not found in order number: {validated_data.orderNumber}"
        )
        return ORJSONResponse(
            content={"detail": "Pièce non trouvée dans la commande."},
            status_code=status.HTTP_404_NOT_FOUND,
        )
    except Exception as e:
        await db.rollback()
        logging.exception(
            f"Exception occurred for order number {validated_data.orderNumber}: {e}"
        )
        return ORJSONResponse(
            content={"detail": "Erreur inconnue lors du scan de la pièce."},
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )


async def batch_scan(
    driver_id: int,
    validated_data: ScannedBatch,
    db: AsyncSession,
) -> ORJSONResponse:
    try:
        stmt = (
            select(DriverOrder)
            .where(
                and_(
                    DriverOrder.order_number == validated_data.orderNumber,
                    DriverOrder.store == validated_data.store,
                    DriverOrder.is_delivered == 0,
                )
            )
            .limit(1)
        )
        result = await db.execute(stmt)
        driver_order = result.scalar_one_or_none()

        if driver_order is None:
            logging.warning(
                f"Driver order not found for order number: {validated_data.orderNumber}"
            )
            return ORJSONResponse(
                content={"detail": "Commande non trouvée."},
                status_code=status.HTTP_404_NOT_FOUND,
            )

        order_info = orjson.loads(driver_order.order_info)

        for item in order_info:
            if item["item"] == validated_data.partNumber:
                item["num_scanned"] = item["units"]
                item["confirmed_scanned"] = item["units"]

                driver_order.order_info = orjson.dumps(order_info).decode("utf-8")

                skipped_part = SkippedPart(
                    driver_id=driver_id,
                    order_number=validated_data.orderNumber,
                    store=validated_data.store,
                    part_number=validated_data.partNumber,
                    created_at=datetime.utcnow(),
                )

                db.add(skipped_part)
                await db.commit()
                await db.refresh(driver_order)

                return ORJSONResponse(
                    content={"num_scanned": item["num_scanned"]},
                    status_code=status.HTTP_200_OK,
                )

        logging.warning(
            f"Part number {validated_data.partNumber} not found in order number: {validated_data.orderNumber}"
        )
        return ORJSONResponse(
            content={"detail": "Pièce non trouvée dans la commande."},
            status_code=status.HTTP_404_NOT_FOUND,
        )
    except Exception as e:
        await db.rollback()
        logging.exception(
            f"Exception occurred for order number {validated_data.orderNumber}: {e}"
        )
        return ORJSONResponse(
            content={"detail": "Erreur inconnue lors du scan de la pièce."},
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )

async def received_by(data: DriverOrderModel.ReceivedBy, db: AsyncSession):
        async with db.begin():
            # Fetch the order by orderNumber
            result = await db.execute(select(DriverOrder).where(DriverOrder.order_number == data.order_number))
            driver_order = result.scalars().first()

            if not driver_order:
                raise HTTPException(status_code=404, detail="Order not found")

            # Update the received_by field
            driver_order.received_by = data.received_by

            # Commit the changes
            await db.commit()

        return {"status": "success", "message": "Received by updated successfully"}