from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from cryptography.hazmat.primitives import serialization
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from dotenv import load_dotenv
from datetime import datetime
import os

# Load the .env file
load_dotenv(
    os.path.join(os.path.dirname(__file__), ".env"),
)

# ---- Environment variables (Database Credentials) ----
STATE_OF_DEPLOYMENT = os.getenv("STATE_OF_DEPLOYMENT")
DB_HOST = os.getenv("DB_HOST")
DB_PORT = os.getenv("DB_PORT")
DB_USER_PRIMARY = os.getenv("DB_USER_PRIMARY")
DB_USER_SECONDARY = os.getenv("DB_USER_SECONDARY")
DB_PASSWORD_PRIMARY = os.getenv("DB_PASSWORD_PRIMARY")
DB_PASSWORD_SECONDARY = os.getenv("DB_PASSWORD_SECONDARY")
DB_DATABASE_PRIMARY = os.getenv("DB_DATABASE_PRIMARY")
DB_DATABASE_SECONDARY = os.getenv("DB_DATABASE_SECONDARY")

# ---- Environment variables (Secret keys) ----
JWE_SECRET_KEY = os.getenv("JWE_SECRET_KEY").encode()
ARGON2_SECRET_KEY = os.getenv("ARGON2_SECRET_KEY")
APP_ADMIN_KEY = os.getenv("APP_ADMIN_KEY")
DISPATCH_ADMIN_KEY = os.getenv("DISPATCH_ADMIN_KEY")
HERE_API_KEY = os.getenv("HERE_API_KEY")
HERE_SDK_KEY_ID = os.getenv("HERE_SDK_KEY_ID")
HERE_SDK_KEY = os.getenv("HERE_SDK_KEY")

# ---- Environment variables (Constant and Default values) ----
DEFAULT_VEHICULE = os.getenv("DEFAULT_VEHICULE")
DEFAULT_IMAGE_FILENAME = os.getenv("DEFAULT_IMAGE_FILENAME")
DEFAULT_DATETIME = datetime.strptime(os.getenv("DEFAULT_DATETIME"), "%Y-%m-%d %H:%M:%S")

SS_EMAIL = os.getenv("SS_EMAIL")
SS_EMAIL_PASSWORD = os.getenv("SS_EMAIL_PASSWORD")

# ---- Environment variables (Asymmetric key) ----
# WARNING: These should be ideally change every 6 to 12 months
with open("utility/private_key.pem", "rb") as key_file:
    PRIVATE_KEY = serialization.load_pem_private_key(
        key_file.read(),
        password=None,
    )
# WARNING: These should be ideally change every 6 to 12 months
with open("utility/public_key.pem", "rb") as key_file:
    PUBLIC_KEY = serialization.load_pem_public_key(
        key_file.read(),
    )

# Create a base class for your models
Base = declarative_base()


# Database URLs
PRIMARY_DATABASE_URL = f"mysql+aiomysql://{DB_USER_PRIMARY}:{DB_PASSWORD_PRIMARY}@{DB_HOST}:{DB_PORT}/{DB_DATABASE_PRIMARY}"
SECONDARY_DATABASE_URL = f"mysql+aiomysql://{DB_USER_SECONDARY}:{DB_PASSWORD_SECONDARY}@{DB_HOST}:{DB_PORT}/{DB_DATABASE_SECONDARY}"


# Create the engine
primary_engine = create_async_engine(
    PRIMARY_DATABASE_URL,
    pool_size=8,
    pool_recycle=21600,
    echo=False,
)

secondary_engine = create_async_engine(
    SECONDARY_DATABASE_URL,
    pool_size=2,
    pool_recycle=21600,
    echo=False,
)


# Create a session factory function for the primary database
def PrimarySessionLocal():
    return AsyncSession(
        bind=primary_engine,
        expire_on_commit=False,
    )


# Create a session factory function for the secondary database
def SecondarySessionLocal():
    return AsyncSession(
        bind=secondary_engine,
        expire_on_commit=False,
    )


# Dependency to get a database session for the primary database
async def get_primary_db() -> AsyncSession:
    async with PrimarySessionLocal() as session:
        yield session


# Dependency to get a database session for the secondary database
async def get_secondary_db() -> AsyncSession:
    async with SecondarySessionLocal() as session:
        yield session

engine_primary = create_async_engine(PRIMARY_DATABASE_URL, echo=True)

# Session factory for the primary database
SessionLocalPrimary = sessionmaker(
    autocommit=False, autoflush=False, bind=engine_primary, class_=AsyncSession
)

# Engine for the secondary database
engine_secondary = create_async_engine(SECONDARY_DATABASE_URL, echo=True)

# Session factory for the secondary database
SessionLocalSecondary = sessionmaker(
    autocommit=False, autoflush=False, bind=engine_secondary, class_=AsyncSession
)