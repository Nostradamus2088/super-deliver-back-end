from sqlalchemy import Column, Integer, String, DateTime
from utility.util import Base
from pydantic import BaseModel
from datetime import datetime

class LocatorLogin(BaseModel):
    username: str
    password: str

class LocatorCreate(BaseModel):
    username: str
    password: str
    store: int

class UsersLocator(Base):
    __tablename__ = "locator_users"
    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(String(255), nullable=False, unique=True)
    password = Column(String(255), nullable=False)
    store = Column(Integer, nullable=False)
    created_at = Column(DateTime, default=datetime.utcnow, nullable=False)
