from sqlalchemy import Column, Integer, String, DateTime, Double
from pydantic import BaseModel
from utility.util import Base
from datetime import datetime


class TelemetryRequest(BaseModel):
    order_number: str
    store: int
    speed: float
    latitude: float
    longitude: float


class RouteDeviationRequest(BaseModel):
    order_number: str
    store: int
    latitude: float
    longitude: float

class RouteRequestData(BaseModel):
    origin: str
    destination: str
    departureTime: str = None

class RouteResponseData(BaseModel):
    routes: list

class Telemetry(Base):
    __tablename__ = "telemetry"
    id = Column(Integer, primary_key=True, autoincrement=True)
    driver_id = Column(Integer, nullable=False)
    order_number = Column(String(32), nullable=False)
    store = Column(Integer, nullable=False)
    speed = Column(Double, nullable=False)
    latitude = Column(Double, nullable=False)
    longitude = Column(Double, nullable=False)
    created_at = Column(Integer, nullable=False)


class RouteDeviation(Base):
    __tablename__ = "route_deviation"
    id = Column(Integer, primary_key=True, autoincrement=True)
    driver_id = Column(Integer, nullable=False)
    order_number = Column(String(32), nullable=False)
    store = Column(Integer, nullable=False)
    latitude = Column(Double, nullable=False)
    longitude = Column(Double, nullable=False)
    created_at = Column(Integer, nullable=False)


class SkippedPart(Base):
    __tablename__ = "skipped_parts"
    id = Column(Integer, primary_key=True, autoincrement=True)
    driver_id = Column(Integer, nullable=False)
    order_number = Column(String(32), nullable=False)
    store = Column(Integer, nullable=False)
    part_number = Column(String(32), nullable=False)
    created_at = Column(DateTime, default=datetime.utcnow, nullable=False)
