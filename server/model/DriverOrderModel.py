from sqlalchemy import Column, Integer, String, Text, DateTime, Double, Boolean
from utility.util import Base, DEFAULT_DATETIME, DEFAULT_IMAGE_FILENAME
from pydantic import BaseModel
from datetime import datetime
from zoneinfo import ZoneInfo

class InvoiceCode(BaseModel):
    invoice_code: str

    @property
    def customer(self) -> str:
        return self.invoice_code[2:8]

    @property
    def order_number(self) -> str:
        return self.invoice_code[11:17]

    @property
    def store(self) -> str:
        return self.invoice_code[19:20]


class InvoiceInfo(BaseModel):
    orderNumber: str
    store: int

class OrderReorderRequest(BaseModel):
    order_number: str
    new_index: int

class ScannedPart(BaseModel):
    orderNumber: str
    store: int
    partCode: str


class ScannedBatch(BaseModel):
    orderNumber: str
    store: int
    partNumber: str

class DriverOrderNumber(BaseModel): 
    order_number: int

class ReceivedBy(BaseModel): 
    order_number: str
    received_by: str

class StartRouteRequest(BaseModel):
    route: str

eastern = ZoneInfo('America/New_York')
def current_time_eastern():
    return datetime.now(eastern)

class DriverOrder(Base):
    __tablename__ = "drivers_orders"
    id = Column(Integer, primary_key=True, autoincrement=True)
    order_number = Column(String(32), nullable=False)
    store = Column(Integer, nullable=False)
    customer = Column(String(32), nullable=False)
    order_info = Column(Text, nullable=False)
    client_name = Column(String(64), nullable=False)
    phone_number = Column(String(32), nullable=False)
    latitude = Column(Double, nullable=False)
    longitude = Column(Double, nullable=False)
    address = Column(String(128), nullable=False)
    ship_addr = Column(String(128), nullable=False)
    driver_id = Column(Integer, nullable=False)
    signature_filename = Column(
        String(384),
        default=DEFAULT_IMAGE_FILENAME,
        nullable=False,
    )
    photo_filename = Column(
        String(384),
        default=DEFAULT_IMAGE_FILENAME,
        nullable=False,
    )
    is_arrived = Column(Boolean, default=False, nullable=False)
    is_delivered = Column(Boolean, default=False, nullable=False)
    created_at = Column(DateTime, default=datetime.utcnow, nullable=False)
    arrived_at = Column(DateTime, default=DEFAULT_DATETIME, nullable=False)
    delivered_at = Column(DateTime, default=DEFAULT_DATETIME, nullable=False)
    updated_at = Column(DateTime, default=current_time_eastern, onupdate=current_time_eastern, nullable=False)
    driver_name = Column(String(32), nullable=False)
    order_index = Column(Integer) 
    route = Column(String(32), nullable=False)
    route_started = Column(Boolean, default=False, nullable=False)
    received_by = Column(String(64), nullable=False)
